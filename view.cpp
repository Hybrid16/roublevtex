//Класс -вид
//Отвечает за создание элементов на сцене и обработку некоторых событий

#include "view.h"
#include <QtGui>
using namespace std;

View::View(QString* latex, QString*A,QString*B,QWidget *parent,QVBoxLayout* mainLayout, QStatusBar *statusBar, KLFBackend::klfInput *input, KLFBackend::klfSettings* settings)
{
    this->settings = settings;
    this->input = input;
    this->statusBar = statusBar;
    this->latex = latex;
    this->A = A;
    this->B = B;
    this->mainLayout= mainLayout;
    this->parent = parent;
    symbol = nullptr;


    splitGroupBox = nullptr;
    stringOfSplit = *new vector<QString *>;
    hboxesOfSplits = *new vector<QHBoxLayout*>;
    labelsOfSplits= *new vector<QLabel*>;
    numbersOfSplit = *new vector<QLabel *>;
    selectProven = *new vector<ClickableLabel *>;
    list = *new QList<QString>;//список исходных посылок из доказываемого утверждения

    labels = *new vector<ClickableLabel *>; //динамический список меток
    isLabelVerified = *new vector<bool>;
    sendsInput = *new vector<QString>;
    hLayouts = *new QList<QHBoxLayout *>;
    vLayouts = *new QList<QVBoxLayout *>;
    tree = nullptr;
    basic = nullptr;
    proofVBox = nullptr;

    aDone = nullptr;
    bDone = nullptr;
    parent->move(0,0);
    currentFocus = -100;
}

//Метод создания окна ввода исходного утверждения
void View::createBasicStatement(){
    QHBoxLayout *hb1 = new QHBoxLayout();
    QLabel *l1 = new QLabel("Input basic statement");

    QPushButton *toolButton = new QPushButton("Tools");
    connect(toolButton, SIGNAL (clicked()), parent, SLOT (tool_clicked()));
    hb1->addWidget(l1);
    hb1->addWidget(toolButton);
    basic= new QLabel();
    basic->setStyleSheet("QLabel{"
                                           "min-width:50px;"
                                         "min-height:40px;"
                                         "max-height:40px;"
                                         "border-style: solid;"
                                         "}");
    QPixmap dot= QPixmap(":/rec/img/dot.png");
    basic->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
    basicDone = new QPushButton("InputDone");
     connect(basicDone, SIGNAL(clicked()),parent,SLOT(basic_done()));
    mainLayout->addLayout(hb1);
    mainLayout->addWidget(basic);
    mainLayout->addWidget(basicDone);
    return;
}

//Метод создания окна ввода тождественныъ преобразований
void View::createIdentificalTransformation(){
    //Создаем панель инструментов
    if(symbol){
        symbol->~symbols();
        symbol =  nullptr;
    }
    symbol = new symbols(parent,A, statusBar);
    connect(symbol, SIGNAL(update(QString*)), parent, SLOT(updateLatex(QString*)));
    connect(symbol,SIGNAL(clear()), parent, SLOT(clear()));
    moveDialog();
    symbol->show();

    QGroupBox *replace = new QGroupBox("Input replace statements");
    QVBoxLayout *replaceVbox = new QVBoxLayout();
    QHBoxLayout *aLayout = new QHBoxLayout();
    aLayout->setAlignment(Qt::AlignLeft);
    QHBoxLayout *bLayout = new QHBoxLayout();
    bLayout->setAlignment(Qt::AlignLeft);
    QLabel *aEqual= new QLabel("A=");
    QLabel *bEqual = new QLabel("B=");
    QFont font = aEqual->font();
    font.setPointSize(19);
    aEqual->setFont(font);
    bEqual->setFont(font);
    aLabel = new QLabel();
    bLabel = new QLabel();
    aDone = new QPushButton();
    bDone = new QPushButton();
    connect(aDone,SIGNAL(clicked()),parent, SLOT(aDone_clicked()));
    connect(bDone,SIGNAL(clicked()),parent,SLOT(bDone_clicked()));
    aDone->setFixedWidth(35);
    aDone->setFixedHeight(30);
    bDone->setFixedWidth(35);
    bDone->setFixedHeight(30);
    aDone->setIcon(QIcon(":/rec/img/done.png"));
    bDone->setIcon(QIcon(":/rec/img/done.png"));
    aLabel->setStyleSheet("QLabel {"//background-color : white;"
                              "min-width:380px;"
                              "min-height:30px;"
                              "max-height:50px;}");
    bLabel->setStyleSheet("QLabel {"//background-color : white;"
                              "min-width:380px;"
                              "min-height:30px;"
                              "max-height:50px;}");
    //Заполняем пустые метки красными точками, обозначаеющие готовность для ввода
    QPixmap dot= QPixmap(":/rec/img/dot.png");
    aLabel->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
    bLabel->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));

    aLayout->addWidget(aEqual);
    aLayout->addWidget(aLabel);
    aLayout->addWidget(aDone);
    bLayout->addWidget(bEqual);
    bLayout->addWidget(bLabel);
    bLayout->addWidget(bDone);

    replaceVbox->addLayout(aLayout);
    replaceVbox->addLayout(bLayout);

    replace->setLayout(replaceVbox);
    mainLayout->addWidget(replace);
}

//Метод перемещает диалоговое окно инструментов в правую часть от главного окна
void View::moveDialog(){
    symbol->move(parent->x() +parent->width(),parent->y());
}

//Метод создания онка разбиения исходного утверждения
void View::createSplittingTheBasicStatement(){
    if(!splitGroupBox)
    {
        splitGroupBox = new QGroupBox("Splitting the basic statement");
        splitgrouplay = new QVBoxLayout;
        splitGroupBox->setLayout(splitgrouplay);
        mainLayout->addWidget(splitGroupBox);
    }

    hboxesOfSplits.push_back(new QHBoxLayout);
    hboxesOfSplits.back()->setAlignment(Qt::AlignLeft);
    labelsOfSplits.push_back(new QLabel);
    stringOfSplit.push_back(new QString(""));
    if(symbol){
        symbol->~symbols();
        symbol =  nullptr;
    }
    symbol = new symbols(parent, stringOfSplit.at(splitCount), statusBar);
    connect(symbol, SIGNAL(update(QString*)), parent, SLOT(updateLatex(QString*)));
    connect(symbol,SIGNAL(clear()), parent, SLOT(clear()));
    moveDialog();
    symbol->show();
    labelsOfSplits.at(splitCount)->setStyleSheet("QLabel {"
                                                 "min-width:420px;"
                                                 "min-height:30px;"
                                                 "max-height:50px;}");
    QPixmap dot= QPixmap(":/rec/img/dot.png");
    labelsOfSplits.back()->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));


    //Создание контейнера с кнопками
    QHBoxLayout *buttonLayout = new QHBoxLayout();
    buttonLayout->setAlignment(Qt::AlignLeft);
    //Кнопка готово
    doneSplit = new QPushButton();
    doneSplit->setFixedWidth(35);
    doneSplit->setFixedHeight(30);
    doneSplit->setIcon(QIcon(":/rec/img/done.png"));
    connect(doneSplit,SIGNAL(clicked()), parent,SLOT(splits_clicked()));
    //Кнопка добавления разбиения
    plus = new QPushButton("+");
    plus->setFixedWidth(35);
    plus->setFixedHeight(30);
    connect(plus,SIGNAL(clicked()),parent,SLOT(plus_clicked()));

    //Добавление слоев
    buttonLayout->addWidget(plus);
    buttonLayout->addWidget(doneSplit);
    splitgrouplay->addLayout(buttonLayout);

    numbersOfSplit.push_back(new QLabel(QString::number(splitCount+1)+")"));
    QFont font = numbersOfSplit.at(splitCount)->font();
    font.setPointSize(19);
    numbersOfSplit.at(splitCount)->setFont(font);

    hboxesOfSplits.at(splitCount)->addWidget(numbersOfSplit.at(splitCount));
    hboxesOfSplits.at(splitCount)->addWidget(labelsOfSplits.at(splitCount));
    splitgrouplay->addLayout(hboxesOfSplits.at(splitCount));
    splitCount++;
}

//Метод создания окна выбора утверждения для последующего доказательства
void View::createChoiceOfStatements(){
    //Создание диалога с выбором введенных разбиений
    dlg = new QDialog(parent);
    dlg->setWindowTitle(tr("The choice of the statement to be proved"));
    QVBoxLayout *selectVbox = new QVBoxLayout();
    for(int i= 0;i<int(stringOfSplit.size());i++){
        selectProven.push_back(new ClickableLabel());
        selectProven.at(i)->setStyleSheet("QLabel{"//background-color : white;"
                                          "min-height:40px;"
                                          "max-height:87px;"
                                          "}");
        selectProven.at(i)->setTag(i);
        connect(selectProven.at(i), SIGNAL(clicked(QMouseEvent*)),parent, SLOT(labelSelectProvenClicked(QMouseEvent*)));
        selectProven.at(i)->setPixmap(*labelsOfSplits.at(i)->pixmap());
        selectVbox->addWidget(selectProven.at(i));
    }
    dlg->setLayout(selectVbox);
    dlg->exec();

    //Запоминаем выбранную метку
    splitlabel = *stringOfSplit.at(selectedFocus);
    //Удаляем ее из множества еще не доказанных разбиений
    stringOfSplit.erase(stringOfSplit.begin()+selectedFocus);
    labelsOfSplits.erase(labelsOfSplits.begin()+selectedFocus);

    list = splitlabel.split("\\rightarrow").at(0).split("\\wedge");
    splitlabel = splitlabel.split("\\rightarrow ").at(1);

    proof = new QGroupBox();
    proofVBox = new QVBoxLayout();

    QLabel *label2  = new QLabel("Proven statement");

    //Если в доказываемом увтерждении имеется конъюнкция условия
    if(list.size()==2){
        //Далее необходимо разделить доказываемое утверждение на 2 части, на которые можно будет кликать
        //Это нужно для того, чтолбы из доказываемого утверждения можно было вести стрелки
        //Символ стрелки и конъюнкции генерируем статическими метками

        //Первая кликабельная часть доказываемого утверждения
        proven = new ClickableLabel();
        //Вторая часть...
        proven2 = new ClickableLabel();
        QLabel *end = new QLabel();
        QLabel *wedge = new QLabel();
        QLabel *arrow = new QLabel();
        arrow->setStyleSheet("QLabel {"
                             "max-width:60px;"
                             "min-height:60px;"
                             "max-height:60px;}");

        input->latex = "\\rightarrow";
        KLFBackend::klfOutput out = KLFBackend::getLatexFormula(*input, *settings);
        arrow->setPixmap(QPixmap::fromImage(out.result).scaled(arrow->geometry().width(),arrow->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

        wedge->setStyleSheet("QLabel {"//background-color : white;"
                             "max-width:30px;"
                             "min-height:30px;"
                             "max-height:30px;}");

        end->setStyleSheet("QLabel {"//background-color : white;"
                              "min-width:30px;"
                              "min-height:30px;"
                              "max-height:30px;}");
        //Назначаем первому условию индекс -1, а второму -2
        proven->setTag(-1);
        proven2->setTag(-2);
        connect(proven2, SIGNAL(clicked(QMouseEvent*)),parent, SLOT(drawLabelClicked(QMouseEvent*)));
        connect(proven, SIGNAL(clicked(QMouseEvent*)),parent, SLOT(drawLabelClicked(QMouseEvent*)));
        proven->setStyleSheet("QLabel {"//background-color : white;"
                              "min-width:30px;"
                              "min-height:30px;"
                              "max-height:30px;}");
        proven2->setStyleSheet("QLabel {"//background-color : white;"
                              "min-width:30px;"
                              "min-height:30px;"
                              "max-height:30px;}");
        input->latex = "\\wedge";
        out = KLFBackend::getLatexFormula(*input, *settings);
        wedge->setPixmap(QPixmap::fromImage(out.result).scaled(wedge->geometry().width(),wedge->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

        //Генерируем latex формулы условий по их строкам
        QString temp = list.at(0);
        input->latex = temp;
        out = KLFBackend::getLatexFormula(*input, *settings);
        QPixmap tPix = QPixmap::fromImage(out.result).scaled(10000,proven->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
        proven->setFixedWidth(tPix.width());
        proven->setPixmap(tPix);

        temp = list.at(1);
        input->latex = temp;
        out = KLFBackend::getLatexFormula(*input, *settings);
        tPix = QPixmap::fromImage(out.result).scaled(10000,proven2->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
        proven2->setFixedWidth(tPix.width());
        proven2->setPixmap(tPix);

        input->latex = splitlabel;
        out = KLFBackend::getLatexFormula(*input, *settings);
        end->setPixmap(QPixmap::fromImage(out.result).scaled(end->geometry().width(),end->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

        QHBoxLayout *provenHBox= new QHBoxLayout();
        provenHBox->addWidget(label2);
        provenHBox->addWidget(proven);
        provenHBox->addWidget(wedge);
        provenHBox->addWidget(proven2);
        provenHBox->addWidget(arrow);
        provenHBox->addWidget(end);

        proofVBox->addLayout(provenHBox);
        proof->setLayout(proofVBox);
        mainLayout->addWidget(proof);
    }
    //Если в доказываемом утверждении только одно условие
    else{
        proven = new ClickableLabel();
        QLabel *end = new QLabel();
        QLabel *arrow = new QLabel();
        arrow->setStyleSheet("QLabel {"
                             "max-width:60px;"
                             "min-height:60px;"
                             "max-height:60px;}");
        input->latex = "\\rightarrow";
        KLFBackend::klfOutput out = KLFBackend::getLatexFormula(*input, *settings);
        arrow->setPixmap(QPixmap::fromImage(out.result).scaled(arrow->geometry().width(),arrow->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
        end->setStyleSheet("QLabel {"
                              "min-width:70px;"
                              "min-height:30px;"
                              "max-height:50px;}");
        proven->setTag(-1);
        proven->setStyleSheet("QLabel {"
                              "min-width:70px;"
                              "min-height:30px;"
                              "max-height:50px;}");
        connect(proven, SIGNAL(clicked(QMouseEvent*)),parent, SLOT(drawLabelClicked(QMouseEvent*)));
        QString temp = list.at(0);
        input->latex = temp;
        out = KLFBackend::getLatexFormula(*input, *settings);
        out = KLFBackend::getLatexFormula(*input, *settings);
        QPixmap tPix = QPixmap::fromImage(out.result).scaled(10000,proven->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
        proven->setFixedWidth(tPix.width());
        proven->setPixmap(tPix);

        input->latex = splitlabel;
        KLFBackend::klfOutput out2 = KLFBackend::getLatexFormula(*input,*settings);
        end->setPixmap(QPixmap::fromImage(out2.result).scaled(end->geometry().width(),end->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
        QHBoxLayout *provenHBox= new QHBoxLayout();
        provenHBox->addWidget(label2);
        provenHBox->addWidget(proven);
        provenHBox->addWidget(arrow);
        provenHBox->addWidget(end);

        proofVBox->addLayout(provenHBox);
        proof->setLayout(proofVBox);
        mainLayout->addWidget(proof);

    }

}

//Метод создания окна ввода начальной посылки и доказательства
void View::createBeginProving(){
    if(symbol){
        symbol->~symbols();
        symbol =  nullptr;
    }
    countAdded++;
    sendsInput.push_back("");
    currentFocus = labels.size();
    symbol = new symbols(parent, &sendsInput.at(currentFocus),statusBar);
    connect(symbol, SIGNAL(update(QString*)), parent, SLOT(updateLatex(QString*)));
    connect(symbol,SIGNAL(clear()), parent, SLOT(clear()));
    moveDialog();
    symbol->show();

    hLayouts<<new QHBoxLayout();
    hLayouts.back()->setSpacing(0);

    isLabelVerified.push_back(false);
    labels.push_back(new ClickableLabel("",parent));
    //Задаем текущей метке красную обводку
    labels.at(currentFocus)->setStyleSheet("QLabel{"
                                "min-width:180px;"
                                "max-width:180px;"
                                                                                              "min-height:30px;"
                                                                                              "max-height:30px;"
                                                                                              "border-style: solid;"
                                                                                              "border-width: 1px;"
                                                                                              "border-color: red; "
                                                                                              "}");
    QPixmap dot= QPixmap(":/rec/img/dot.png");
    labels.back()->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
    labels.back()->setTag(currentFocus);
    connect(labels.back(), SIGNAL(clicked(QMouseEvent*)),parent, SLOT(labelClicked(QMouseEvent*)));
    hLayouts.back()->setAlignment(Qt::AlignLeft);
    hLayouts.back()->addWidget(labels.back());

    //Добавление элементов на сцену
    QHBoxLayout *horizonProof = new QHBoxLayout();
    horizonProof->addLayout(hLayouts.back());

    QHBoxLayout *addLay= new QHBoxLayout();
    comboBox = new QComboBox();
    comboBox->addItem("->");
    comboBox->addItem("->||2x");
    comboBox->addItem("->||3x");
    comboBox->addItem("->{2x");
    comboBox->addItem("->{3x");
    add = new QPushButton("Add");
    connect(add,SIGNAL(clicked()),this,SLOT(add_clicked()));
    addLay->addWidget(comboBox);
    addLay->addWidget(add);
    proofVBox->addLayout(addLay);
    proofVBox->addLayout(horizonProof);
    verify = new QPushButton("Verify");
    connect(verify,SIGNAL(clicked()), parent, SLOT(done1_clicked()));
    proofVBox->addWidget(verify);

    draw = new QPushButton("Draw");
    connect(draw,SIGNAL(clicked()),parent, SLOT(drawAline()));
    proofVBox->addWidget(draw);

    //Если дерево пустое, то заводим его
    if(!tree){
        tree= new node();
        tree->father = nullptr;
        tree->labelID=0;
        //Заводим узлы дерева для доказываемых условий, но не помещаем их в дерево
        if(list.size()==2){
            proven2Node = new  node();
            QString t = list.at(1);
            Parser *parser = new Parser(&t,statusBar);
            proven2Node->value = parser->getOPZ();
        }
        provenNode = new node();
        QString t = list.at(0);
        Parser *parser = new Parser(&t,statusBar);
        provenNode->value = parser->getOPZ();
        isLabelVerified.push_back(false);
    }
}

//Метод, вызываемый при нажатии на кнопку добавления посылок
void View::add_clicked()
{
    //Получаем индекс последней непроверенной метки
    int index = findNonVerified();
    //Если пользователь нажал кнопку добавления, но еще не проверил предыдущие посылки
    if(index<labels.size()){
        statusBar->showMessage("Can't add premise, because of previous doesn't virified.");
        return;
    }
    if(comboBox->currentText() == "->"){ //Если просто посылка без разветвления
        //Заносим новый лист в дерево
        tree->at(currentFocus)->centerSon = new node();
        tree->at(currentFocus)->centerSon->father = tree->at(currentFocus);
        tree->at(currentFocus)->centerSon->labelID = labels.size()&INT_MAX;

        //Создаем новое окно для ввода посылки
        QLabel *implication= new QLabel();
        implication->setStyleSheet("QLabel {"
                                   "min-width:40px;"
                                   "max-width:40px;"
                                   "min-height:30px;"
                                   "max-height:30px;}");
        input->latex = "\\rightarrow";
        KLFBackend::klfOutput out = KLFBackend::getLatexFormula(*input, *settings);
        implication->setPixmap(QPixmap::fromImage(out.result).scaled(implication->geometry().width(),implication->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

        hLayouts<<new QHBoxLayout;
        hLayouts.back()->setAlignment(Qt::AlignLeft);

        labels.push_back(new ClickableLabel("",parent));
        labels.back()->setTag((labels.size()-1));
        isLabelVerified.push_back(false);
        sendsInput.push_back("");
        labels.back()->setStyleSheet("QLabel{"
                                     "min-width:180px;"
                                     "max-width:180px;"
                                     "min-height:30px;"
                                     "max-height:30px;"
                                     "}");
        labels.back()->setFixedWidth(180);
        QPixmap dot= QPixmap(":/rec/img/dot.png");
        labels.back()->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
        labels.back()->setTag((labels.size()-1));
        connect(labels.back(), SIGNAL(clicked(QMouseEvent*)),parent, SLOT(labelClicked(QMouseEvent*)));

        hLayouts.at(currentFocus)->addLayout(hLayouts.back());
        hLayouts.back()->addWidget(implication);
        hLayouts.back()->addWidget(labels.back());
    }
    else if(comboBox->currentText() == "->||2x" ||comboBox->currentText() == "->{2x"){ //Если выбрано разветвление на 2 ветки
        //Создаем и связываем 2 листа
        tree->at(currentFocus)->upperSon= new node();
        tree->at(currentFocus)->upperSon->labelID=labels.size()&INT_MAX;
        tree->at(currentFocus)->downSon = new node();
        tree->at(currentFocus)->downSon->labelID= (labels.size()&INT_MAX)+1;
        tree->at(currentFocus)->upperSon->brothers.push_back(tree->at(currentFocus)->downSon);
        tree->at(currentFocus)->downSon->brothers.push_back(tree->at(currentFocus)->upperSon);
        tree->at(currentFocus)->upperSon->father= tree->at(currentFocus);
        tree->at(currentFocus)->downSon->father = tree->at(currentFocus);

        if(comboBox->currentText() == "->||2x"){

            tree->at(currentFocus)->bracket= "->||";

            bracket = new QLabel();
            bracket->setStyleSheet("QLabel {"
                                   "min-width:50px;"
                                   "max-width:50px;"
                                   "min-height:30px;"
                                   "max-height:70px;"
                                   "}");

            input->latex = "\\rightarrow \\Bigg\\lVert";
            KLFBackend::klfOutput out = KLFBackend::getLatexFormula(*input, *settings);
            QPixmap temp = QPixmap::fromImage(out.result).scaled(10000,bracket->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
            bracket->setFixedWidth(temp.width());
            bracket->setPixmap(temp);
            countBranches+=1;
        }
        else{
            tree->at(currentFocus)->bracket= "->{";
            bracket= new QLabel();
            bracket->setStyleSheet("QLabel {"
                                   "min-width:50px;"
                                   "max-width:50px;"
                                   "min-height:30px;"
                                   "max-height:70px;"
                                   "}");
            input->latex = "\\rightarrow \\Bigg\\{";
            KLFBackend::klfOutput out = KLFBackend::getLatexFormula(*input, *settings);
            QPixmap temp = QPixmap::fromImage(out.result).scaled(10000,bracket->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
            bracket->setFixedWidth(temp.width());
            bracket->setPixmap(temp);
        }
        //Заносим элементы на сцену
        vLayouts<<new QVBoxLayout;
        vLayouts.back()->setSpacing(0);

        labels.push_back(new ClickableLabel("",parent));
        labels.push_back(new ClickableLabel("",parent));
        isLabelVerified.push_back(false);
        isLabelVerified.push_back(false);

        sendsInput.push_back("");
        sendsInput.push_back("");

        hLayouts<<new QHBoxLayout;
        hLayouts.back()->setAlignment(Qt::AlignLeft);
        hLayouts<<new QHBoxLayout;
        hLayouts.back()->setAlignment(Qt::AlignLeft);

        QPixmap dot= QPixmap(":/rec/img/dot.png");

        for(int i =int(labels.size()-2); i<int(labels.size()); i++){
            labels.at(i)->setStyleSheet("QLabel{"
                                            "min-height:30px;"
                                            "max-height:30px;"
                                            "}");

            labels.at(i)->adjustSize();
            labels.at(i)->setTag(i);

            connect(labels.at(i), SIGNAL(clicked(QMouseEvent*)),parent, SLOT(labelClicked(QMouseEvent*)));
        }
        labels.at(labels.size()-2)->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
        labels.at(labels.size()-2)->setFixedWidth(30);
        labels.back()->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
        labels.back()->setFixedWidth(30);


        hLayouts.at(currentFocus)->addWidget(bracket);
        hLayouts.at(currentFocus)->addLayout(vLayouts.back());
        vLayouts.back()->addLayout(hLayouts.at(hLayouts.size()-2));
        vLayouts.back()->addLayout(hLayouts.back());
        hLayouts.at(hLayouts.size()-2)->addWidget(labels.at(labels.size()-2));
        hLayouts.back()->addWidget(labels.back());


    }
    else{ //Если разбиение на 3 ветки. ТО аналогично

        tree->at(currentFocus)->upperSon= new node();
        tree->at(currentFocus)->upperSon->labelID=labels.size()&INT_MAX;
        tree->at(currentFocus)->centerSon= new node();
        tree->at(currentFocus)->centerSon->labelID=(labels.size()&INT_MAX)+1;
        tree->at(currentFocus)->downSon = new node();
        tree->at(currentFocus)->downSon->labelID= (labels.size()&INT_MAX)+2;

        tree->at(currentFocus)->upperSon->brothers.push_back(tree->at(currentFocus)->centerSon);
        tree->at(currentFocus)->upperSon->brothers.push_back(tree->at(currentFocus)->downSon);

        tree->at(currentFocus)->centerSon->brothers.push_back(tree->at(currentFocus)->upperSon);
        tree->at(currentFocus)->centerSon->brothers.push_back(tree->at(currentFocus)->downSon);

        tree->at(currentFocus)->downSon->brothers.push_back(tree->at(currentFocus)->upperSon);
        tree->at(currentFocus)->downSon->brothers.push_back(tree->at(currentFocus)->centerSon);

        tree->at(currentFocus)->upperSon->father= tree;
        tree->at(currentFocus)->centerSon->father= tree;
        tree->at(currentFocus)->downSon->father = tree;
        if(comboBox->currentText() == "->||3x"){

            tree->at(currentFocus)->bracket= "->||";

            bracket = new QLabel();
            bracket->setStyleSheet("QLabel {"
                                   "min-width:60px;"
                                   "max-width:60px;"
                                   "min-height:100px;"
                                   "max-height:100px;"
                                   "}");

            input->latex = "\\rightarrow \\Bigg\\lVert";
            KLFBackend::klfOutput out = KLFBackend::getLatexFormula(*input, *settings);
            QPixmap temp = QPixmap::fromImage(out.result).scaled(10000,bracket->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
            bracket->setFixedWidth(temp.width());
            bracket->setPixmap(temp);
            countBranches+=2;
        }
        else{
            tree->at(currentFocus)->bracket= "->{";
            bracket= new QLabel();
            bracket->setStyleSheet("QLabel {"
                                   "min-width:60px;"
                                   "max-width:60px;"
                                   "min-height:100px;"
                                   "max-height:100px;"
                                   "}");
            input->latex = "\\rightarrow \\Bigg\\{";
            KLFBackend::klfOutput out = KLFBackend::getLatexFormula(*input, *settings);
            QPixmap temp = QPixmap::fromImage(out.result).scaled(10000,bracket->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
            bracket->setFixedWidth(temp.width());
            bracket->setPixmap(temp);
        }
        vLayouts<<new QVBoxLayout;
        vLayouts.back()->setSpacing(0);

        labels.push_back(new ClickableLabel("",parent));
        labels.push_back(new ClickableLabel("",parent));
        labels.push_back(new ClickableLabel("",parent));
        isLabelVerified.push_back(false);
        isLabelVerified.push_back(false);
        isLabelVerified.push_back(false);

        sendsInput.push_back("");
        sendsInput.push_back("");
        sendsInput.push_back("");

        hLayouts<<new QHBoxLayout;
        hLayouts.back()->setAlignment(Qt::AlignLeft);
        hLayouts<<new QHBoxLayout;
        hLayouts.back()->setAlignment(Qt::AlignLeft);
        hLayouts<<new QHBoxLayout;
        hLayouts.back()->setAlignment(Qt::AlignLeft);

        QPixmap dot= QPixmap(":/rec/img/dot.png");


        for(int i =int(labels.size()-3); i<int(labels.size()); i++){
            labels.at(i)->setStyleSheet("QLabel{"
                                            "min-height:30px;"
                                            "max-height:30px;"
                                            "}");

            labels.at(i)->adjustSize();
            labels.at(i)->setTag(i);

            connect(labels.at(i), SIGNAL(clicked(QMouseEvent*)),parent, SLOT(labelClicked(QMouseEvent*)));
        }
        labels.at(labels.size()-3)->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
        labels.at(labels.size()-3)->setFixedWidth(30);
        labels.at(labels.size()-2)->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
        labels.at(labels.size()-2)->setFixedWidth(30);
        labels.back()->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));
        labels.back()->setFixedWidth(30);


        hLayouts.at(currentFocus)->addWidget(bracket);
        hLayouts.at(currentFocus)->addLayout(vLayouts.back());
        vLayouts.back()->addLayout(hLayouts.at(hLayouts.size()-3));
        vLayouts.back()->addLayout(hLayouts.at(hLayouts.size()-2));
        vLayouts.back()->addLayout(hLayouts.back());
        hLayouts.at(hLayouts.size()-3)->addWidget(labels.at(labels.size()-3));
        hLayouts.at(hLayouts.size()-2)->addWidget(labels.at(labels.size()-2));
        hLayouts.back()->addWidget(labels.back());
    }

}

//Метод нахождения индекса первой непроверенной метки
int View::findNonVerified(){
    for(int i=0; i<int(isLabelVerified.size());i++){
        if(!isLabelVerified.at(i))
            return (i);
    }
    return -1;
}
