#-------------------------------------------------
#
# Project created by QtCreator 2013-04-06T08:57:35
#
#-------------------------------------------------

QT       += core gui\
            widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
VERSION = 0.2

DEFINES += KLF_SRC_BUILD

TARGET = TinyTex
TEMPLATE = app


SOURCES += main.cpp\
    grammar.cpp \
        mainwindow.cpp \
    paintwidget.cpp \
    symbols.cpp \
    parser.cpp \
    clickablelabel.cpp \
    node.cpp \
    view.cpp

HEADERS  += mainwindow.h \
    grammar.h \
    paintwidget.h \
    symbols.h \
    parser.h \
    clickablelabel.h \
    node.h \
    view.h

FORMS    += mainwindow.ui \
    symbols.ui

# note that in unix(linux) systems library names are case sensitive
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/build-KLFBackend/ -lklfbackend
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/build-KLFBackend/ -lklfbackend
else:unix: LIBS += -L$$PWD/build-KLFBackend/ -lKLFBackend

INCLUDEPATH += $$PWD/klfbackend

RESOURCES += \
    resources.qrc


