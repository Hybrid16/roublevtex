//Наследник класса widget с переопределнным методом рисования (paint event)
#include "paintwidget.h"

//Метод рисования стрклок в окне доказательства
void PaintWidget::paintEvent(QPaintEvent *e){
    //Конфигурация пера и инструмена рисования
    QWidget::paintEvent(e);
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    pen.setWidth(5);
    pen.setColor(QColor(255,255,255));
    painter.setPen(pen);
    painter.setPen(Qt::red);

    for (int i = 0;i< tempArrowPathes->size(); i++){
        tempArrowPathes->at(i) = new QPainterPath();
        tempArrowPathes->at(i)->moveTo(tempStartArrows->at(i)->x(),tempStartArrows->at(i)->y());
        tempArrowPathes->at(i)->quadTo(*tempCenters->at(i),*tempEndArrows->at(i));
        tempArrowPathes->at(i)->addEllipse(*tempCenters->at(i),5,5);
        painter.drawPath(*tempArrowPathes->at(i));
    }
    painter.setPen(Qt::white);
    for (int i = 0;i<arrowPathes->size(); i++){
        arrowPathes->at(i)->moveTo(startArrows->at(i)->x(),startArrows->at(i)->y());
        arrowPathes->at(i)->quadTo(*centers->at(i),*endArrows->at(i));
        painter.drawPath(*arrowPathes->at(i));
    }
    //Часть кода для рисования временных стрелок
    for(int i=0;i<tempStartArrows->size();i++)
    {
        //Рисование треугольника на конце срелки
        QLine *line = new QLine(*tempCenters->at(i),*tempEndArrows->at(i));
        double angle = GetAngle(line->dx(),line->dy());
          if (line->dy() >= 0.0) angle = (1.0 * 3.14) + angle;
          angle+=3.14;

          const double sz = 10.0; //pixels
          {
            const QPointF p0 = line->p2();
            const QPointF p1
              = p0 + QPointF(
                 sin(angle + 3.14 + (3.14 * 0.1)) * sz,
                -cos(angle + 3.14 + (3.14 * 0.1)) * sz);
            const QPointF p2
              = p0 + QPointF(
                 sin(angle + 3.14 - (3.14 * 0.1)) * sz,
                -cos(angle + 3.14 - (3.14 * 0.1)) * sz);
            painter.drawPolygon(QPolygonF() << p0 << p1 << p2);
          }
    }
    //Частть кода для рисования постоянных стрелок
    for(int i=0;i<startArrows->size();i++)
    {
        QLine *line = new QLine(*centers->at(i),*endArrows->at(i));
        double angle = GetAngle(line->dx(),line->dy());
          if (line->dy() >= 0.0) angle = (1.0 * 3.14) + angle;
          angle+=3.14;

          const double sz = 10.0; //pixels
          {
              //Рисование треугольника на конце срелки
            const QPointF p0 = line->p2();
            const QPointF p1
              = p0 + QPointF(
                 sin(angle + 3.14 + (3.14 * 0.1)) * sz,
                -cos(angle + 3.14 + (3.14 * 0.1)) * sz);
            const QPointF p2
              = p0 + QPointF(
                 sin(angle + 3.14 - (3.14 * 0.1)) * sz,
                -cos(angle + 3.14 - (3.14 * 0.1)) * sz);
            painter.drawPolygon(QPolygonF() << p0 << p1 << p2);
          }
    }

    this->update();

}
//Метод смещения стрелки при прокрутке колеса мыши
void PaintWidget::wheelEvent(QWheelEvent *event)
{
    int numDegrees = event->delta()/8;
    if(!tempCenters->empty()){
        tempCenters->back()->setX(tempCenters->back()->x()-numDegrees);
        tempCenters->back()->setY(tempCenters->back()->y()+numDegrees);
    }
    this->update();
    event->accept();
}
//Метод смещения стрелки при нажатии мыши
void PaintWidget::mousePressEvent(QMouseEvent *event)
 {
     if (event->button() == Qt::LeftButton && !tempArrowPathes->empty()){ //&& arrowPathes.back()->contains(event->pos())){
         for (int i = 0; i<tempArrowPathes->size();i++)
             if(tempArrowPathes->at(i)->contains(event->pos()))
                 isLine = i;
     }
     else return;
 }

//Метод смещения стрелки при нажатии и перемещении мыши
void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (!(event->buttons() & Qt::LeftButton))
        return;
    if(isLine>-1){
        try {
            *tempCenters->at(isLine) =event->pos();
        } catch (exception e) {
            qDebug()<<"Ошибка 1235";
        }
        this->update();
        event->accept();
    }

}

void PaintWidget::mouseReleaseEvent(QMouseEvent *e)
{
  if(isLine)
      isLine = false;
}

double PaintWidget::GetAngle(const double dx, const double dy)
{
  return 3.14 - (atan(dx/dy));
}

//Метод вызываемый пр нажатии кнопки "Draw" в окне доказательства
void PaintWidget::drawAline(){
    isDrawSelect = !isDrawSelect;
    if(isDrawSelect){
        view->add->setEnabled(false);
        view->currentFocus= -100;
        //Переопределение сигналов меток
        for(int i =0; i<int(view->labels.size()); i++){
            disconnect(view->labels.at(i), SIGNAL(clicked(QMouseEvent*)),mw, SLOT(labelClicked(QMouseEvent*)));
            connect(view->labels.at(i), SIGNAL(clicked(QMouseEvent*)),this, SLOT(drawLabelClicked(QMouseEvent*)));
        }
        disconnect(view->proven, SIGNAL(clicked(QMouseEvent*)),mw, SLOT(labelClicked(QMouseEvent*)));
        connect(view->proven, SIGNAL(clicked(QMouseEvent*)),this, SLOT(drawLabelClicked(QMouseEvent*)));
        if(view->splitlabel.size()==2){
            disconnect(view->proven2, SIGNAL(clicked(QMouseEvent*)),mw, SLOT(labelClicked(QMouseEvent*)));
            connect(view->proven2, SIGNAL(clicked(QMouseEvent*)),this, SLOT(drawLabelClicked(QMouseEvent*)));
        }
    }
    else {
        view->add->setEnabled(true);
        drawLabelsId->push_back(preCurrent);
        drawLabelsId->push_back(view->currentFocus);
       for(int i =0; i<int(view->labels.size()); i++){
           disconnect(view->labels.at(i), SIGNAL(clicked(QMouseEvent*)),this, SLOT(drawLabelClicked(QMouseEvent*)));
           connect(view->labels.at(i), SIGNAL(clicked(QMouseEvent*)),mw, SLOT(labelClicked(QMouseEvent*)));
       }
       disconnect(view->proven, SIGNAL(clicked(QMouseEvent*)),this, SLOT(drawLabelClicked(QMouseEvent*)));
       connect(view->proven, SIGNAL(clicked(QMouseEvent*)),mw, SLOT(labelClicked(QMouseEvent*)));
       if(view->splitlabel.size()==2){
           disconnect(view->proven2, SIGNAL(clicked(QMouseEvent*)),this, SLOT(drawLabelClicked(QMouseEvent*)));
           connect(view->proven2, SIGNAL(clicked(QMouseEvent*)), mw, SLOT(labelClicked(QMouseEvent*)));
       }
       for(int i = 0; i< int( view->labels.size());i++)
           if(i!=view->currentFocus)
               view->labels.at(i)->setStyleSheet("QLabel{"
                                                    "min-height:30px;"
                                                    "max-height:30px;"
                                                    "}");
       if(view->currentFocus!=-1)
           view->proven->setStyleSheet("QLabel{"
                                 "min-height:30px;"
                                 "max-height:30px;"
                                 "}");
       if(view->splitlabel.size() == 2)
           if(view->currentFocus!=-2  )
               view->proven2->setStyleSheet("QLabel{"
                                 "min-height:30px;"
                                 "max-height:30px;"
                                 "}");
       for(int i = 0; i<tempArrowPathes->size(); i++){
           startArrows->push_back(tempStartArrows->at(i));
           endArrows->push_back(tempEndArrows->at(i));
           arrowPathes->push_back(new QPainterPath());
           centers->push_back(tempCenters->at(i));

           tempCenters->clear();
           tempEndArrows->clear();
           tempStartArrows->clear();
           tempArrowPathes->clear();
       }

    }

}

//Метод обработки нажатия на метку с посылкой в окне доказаельства
void PaintWidget::drawLabelClicked(QMouseEvent* e){
    if(view->currentFocus!=-100){
        preCurrent = view->currentFocus;
        preCurCoord = curCoord;
    }
    view->currentFocus=((ClickableLabel *)sender())->tag();
    curCoord =this->mapFromGlobal(e->globalPos());
    if(view->currentFocus!=-1 && view->currentFocus!= -2)
        view->labels.at(view->currentFocus)->setStyleSheet("QLabel{"
                                                              "min-height:30px;"
                                                              "max-height:30px;"
                                                              "border-style: solid;"
                                                              "border-width: 1px;"
                                                              "border-color: red; "
                                                         "}");
    else if(view->currentFocus==-1)
        view->proven->setStyleSheet("QLabel{"
                                                                                                              "min-height:30px;"
                                                                                                              "max-height:30px;"
                                                                                                              "border-style: solid;"
                                                                                                              "border-width: 1px;"
                                                                                                              "border-color: red; "
                                                                                                         "}");
    else if(view->proven2)
        if(view->currentFocus==-2)
            view->proven2->setStyleSheet("QLabel{"//background-color : white;"
                                                                                              "min-height:30px;"
                                                                                              "max-height:30px;"
                                                                                              "border-style: solid;"
                                                                                              "border-width: 1px;"
                                                                                              "border-color: red; "
                                                                                         "}");
    if(preCurrent!=-100)
        if(preCurrent!=-1 && preCurrent!=-2)
            view->labels.at(preCurrent)->setStyleSheet("QLabel{"
                                                              "min-height:30px;"
                                                              "max-height:30px;"
                                                              "border-style: solid;"
                                                              "border-width: 1px;"
                                                              "border-color: red; "
                                                              "}");
        else {if(preCurrent==-1)
            view->proven->setStyleSheet("QLabel{"
                                                              "min-height:30px;"
                                                              "max-height:30px;"
                                                              "border-style: solid;"
                                                              "border-width: 1px;"
                                                              "border-color: red; "
                                                              "}");
        }
        else if(view->splitlabel.size() == 2)
                view->proven2->setStyleSheet("QLabel{"
                                                                                                  "min-height:30px;"
                                                                                                  "max-height:30px;"
                                                                                                  "border-style: solid;"
                                                                                                  "border-width: 1px;"
                                                                                                  "border-color: red; "
                                                                                                  "}");
    for(int i = 0; i<int( view->labels.size());i++)
        if(i!=view->currentFocus && i!=preCurrent)
            view->labels.at(i)->setStyleSheet("QLabel{"
                                                 "min-height:30px;"
                                                 "max-height:30px;"
                                                 "}");
    if(view->currentFocus!=-1 && preCurrent!=-1)
        view->proven->setStyleSheet("QLabel{"
                              "min-height:30px;"
                              "max-height:30px;"
                              "}");
    if(view->proven2)
        if(view->currentFocus!=-2 && preCurrent!=-2)
            view->proven2->setStyleSheet("QLabel{"
                              "min-height:30px;"
                              "max-height:30px;"
                              "}");
    if(preCurrent!=-100){
        tempArrowPathes->clear();
        tempStartArrows->clear();
        tempEndArrows->clear();
        tempCenters->clear();
        tempArrowPathes->push_back(new QPainterPath);
        QPoint st;//Временная точка начала
        st = preCurCoord;
        tempStartArrows->push_back(new QPoint(st.x(),st.y()));
        tempArrowPathes->back()->moveTo(st);

        QPoint en;
        en = curCoord;

        tempEndArrows->push_back(new QPoint(en.x(),en.y()));
        QLine *l = new QLine(st,en);
        z = l->center();
        tempCenters->push_back(new QPoint(z.x(),z.y()));
    }
}

