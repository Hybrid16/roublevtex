#ifndef node_H
#define node_H
#include <iostream>
#include <QString>
//using namespace std;
#include <QList>

class node
{
private:

public:
    node();
    QString value;//Значение узла
    node *father;//Указатель на отца узла
    std::vector<node *>brothers;//Вектор указательей на братьев узла
    node *upperSon;//Указатель на верхнего сына
    node *centerSon;//Указаель на центрального сына
    node *downSon;//Указательно на нижнего сына
    QString bracket = ""; //Значение скобки узла при разветвлении(если оно есть) { или ||
    int labelID;//Индекс метки, соответвующей данному узлу в окне доказаттельсва
    node* findLastNode(int index);//Находит последний лист заданного номера метки
    node* at(int);//Метод обращение к узлам дерева по меткам
    node * inputArrow;//Указатель на узел, от которого проведена стрелка в текущий узел
    std::vector<node *> outputArrows;//Множество узлов в которые проведена стрелка из текущего узла

};

#endif
