//#pragma once
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QClipboard>
#include "klfbackend.h"
#include "klfpreviewbuilderthread.h"
#include "symbols.h"
#include <QObject>
#include "clickablelabel.h"
#include <QHBoxLayout>
#include <node.h>
#include <QGroupBox>
#include <QComboBox>
#include <QtGui>
#include <QtCore>
#include <QLine>
#include <QPainter>
#include "view.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    friend class InputDialog;
    ~MainWindow();
signals: void sizeChanged(QSize);
    void draw_clicked();
protected slots:
    void updatePreviewBuilderThreadInput();
    void showRealTimePreview(const QImage& preview, bool latexerror);
    void updateLatex(QString*);//Функция компиляции latex по переданной строке
    void createInputDialog();
    void tool_clicked();//Обработка нажатия на кнопку вызова панели инструментов
    void clear();//Метод, вызываемый при нажатии на кнопку "очистить" Удаляем текущее содержимое строки и поля ввода
    void basic_done();//Метод, вызываемый при нажатии на кнопку проверки исходного утверждения (Зеленой галки
    void aDone_clicked();//Метод вызывается при нажатии пользователем кнопки завершения ввода замены утверждения (A)
    void bDone_clicked();//Аналогичный метод для проверки B
    void plus_clicked();
    void splits_clicked();//Метод, вызываемый при нажатии пользователем кнопки по завершении ввода всех разбиений исходного множества

    void labelClicked(QMouseEvent*); //слот для приёма сигналов от метки
    void labelSelectProvenClicked (QMouseEvent *event);
    void drawAline();

    void done1_clicked();//Метод, вызываемый при нажатии кнопки "Готово" по окончании доказательства
    bool checkbool(int);//Метод осуществляет проверку узлов уже построенного дерева на предыдущем шаге
    bool checkPremise(QString check);
    void unCheckLabel();//Убирает красную рамку после доказательства.
    void end_of_branch();//Метод удаляет лишние элементы со сцены по завершении доказательства
protected:
    View *view;//Объект класса view
    Ui::MainWindow *ui;
    KLFPreviewBuilderThread *mPreviewBuilderThread;
    KLFBackend::klfInput input; //Объект, возврашающий latex-формулу по строке
    KLFBackend::klfSettings settings; //Конфигурации настроей latex-компилятора
    KLFBackend::klfOutput output;

    QString latex;//Строка исходного утверждения для последующей компиляции в Latex формулу
    QString A,B;//Строки для компиляции в Latex замен утверждений A и B 

    QPixmap pixmap;
    bool isNeg = false; //Показывает открыто ли на текущий момент отрицание(дополнение) при вводе пользователем

    QPixmap changeColor(QPixmap,char);//Метод изменяет цвет шрифта сгенерированного latex-изображения
    QString te;//временная строка разбиения исходного утверждения

    vector<int> drawLabelsId;//В данном множетстве хранятся индексы полей(labels), между которыми проведена стрелка
    //Например стрелка идет из поля с индексом 0 в поле с индексом 2
    //То в drawlabelId будет лежать 0,2
    //Значения нужно обрабатывать парами

    int preCurrent =-100; //Индекс предтекущего элемнта в дереве
    int countSquare = 0;//Количество квадратов(концов веток доказательства)

    QStringList items;//СТроки в кобмомбоксе диалога
    QString condition;// Строка полученная выделением уловия(не равенства множеств) из исходного утверждения

    vector<QString> splitsStringsFromBasic; //Множество разбиения исходного утверждения на строки (6)(7)(8)(9) для проверки

    //A и B до преобразования (до первого =)
    QString A_old;
    QString B_old;


    int maxheight;//Максимаяльная высота экрана
    QPoint size_window;//Размер окна
    QString transforms2; //Часть исходного утверждения, которая предполагается для замена на высказываение B
};

#endif
