#ifndef SYMBOLS_H
#define SYMBOLS_H

#include <QDialog>
#include <QStatusBar>
#include "parser.h"

namespace Ui {
class symbols;
}

class symbols : public QDialog
{
    Q_OBJECT

public:
    explicit symbols(QWidget *parent, QString *latex = nullptr,QStatusBar *statusbar=nullptr);
    ~symbols();
public slots:

signals:
    void update(QString*);
    void clear();

private slots:
    void addSymbol(QString);//Метод добавляет нужный символ к строке, проверяет условие отрицаний и вызываетт сигнал
    void on_X1_clicked();
    void on_X2_clicked();
    void on_X3_clicked();
    void on_inter_clicked();
    void on_unite_clicked();
    void on_rightarrow_clicked();
    void on_leftrightarrow_clicked();
    void on_belong_clicked();
    void on_sub_clicked();
    void on_negative_clicked();
    void on_close_clicked();
    void on_lbrace_clicked();
    void on_rbrace_clicked();
    void on_equal_clicked();
    void on_zero_clicked();
    void on_universal_clicked();
    void on_clear_clicked();//Метод очистки выражения, вызывает сигнла clear
    void negativeCheck(QString);//Проверяет, есть ли открытые отрицания(дополнения) и если есть, то редактирует строку latex соответсвующим образом
    void on_pushButton_clicked();
    void on_Cap_clicked();
    void on_removeCharacter_clicked();
    int countOfNearBrackets(QString*);
    void createMessage();//Создает сообщение статус бара
    void on_exists_clicked();
    void on_xButton_clicked();
    void on_doubledots_clicked();
    void on_notin_clicked();
    void on_aButton_clicked();
    void on_bButton_clicked();
    void on_all_clicked();
    void on_square_clicked();
    void on_blacksquare_clicked();
    void on_dotcoma_clicked();
    void on_subsetec_button_clicked();

private:
    Ui::symbols *ui;
    QString *latex;
    int countSub = 0;//Количество открытых отрицаний
    QStatusBar *statusBar;
};

#endif // SYMBOLS_H
