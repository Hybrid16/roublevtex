//Главный класс приложения. Здесь находятся основные обработчики событий кнопок и основные методы
//Программа в качестве back-end вызывает компилятор latex формул,
//В который подается строка с командами, а он, в свою очередь, возвращает изображение с нужной формулой.
//Строка для компиляции генерируется при нажатии на нужные клавиши в классе Symbols

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "klfbackend.h"
#include <QDebug>
#include <QResizeEvent>
#include "parser.h"
#include <QRgb>
#include <QColor>
#include <QtGui>
#include <math.h>
#include <QInputDialog>
#include<QThread>
#include "grammar.h"
#include <QApplication>
#include "paintwidget.h"

//Константа для приведения unsigned long к int
#define INT &INT_MAX

//Инициальзация перемнных в конструкторе
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    input.mathmode = "\\[\\begin{array}{l} ... \\end{array}\\]";
    input.fg_color = qRgb(255,255,255);
    input.bg_color = Qt::black;
    input.dpi = 1200;
    input.preamble = QString("\\usepackage{amssymb,amsmath,mathrsfs}");
    if(!KLFBackend::detectSettings(&settings)) {
        settings.latexexec ="/Library/TeX/texbin/latex";
        settings.dvipsexec = "/Library/TeX/texbin/dvips";
        settings.gsexec = "/usr/local/bin/gs";
        qDebug() << "unable to find LaTeX in default directories.";
    } else {
        qDebug() << "default settings working!";
    }
    // setup variables:
    mPreviewBuilderThread = new KLFPreviewBuilderThread(this, input, settings);
    // make connections
    connect(mPreviewBuilderThread, SIGNAL(previewAvailable(const QImage&, bool)),
        this, SLOT(showRealTimePreview(const QImage&, bool)), Qt::QueuedConnection);

    ui->statusBar->showMessage("Waiting for input...");

    QScrollArea *scrollarea = new QScrollArea;
    scrollarea->setWidgetResizable(true);
    QVBoxLayout *scrolllayout= new QVBoxLayout(scrollarea);

    QVBoxLayout temp;
    view = new View(&latex,&A,&B,this,scrolllayout,ui->statusBar,&input,&settings);

    //Здесь задается список элементов диалогового окна выбора действий пользователем
    items << tr("Enter the basic statement") << tr("Splitting the basic statement") << tr("Inputting identical transformations for some sets") << tr("The choice of the statement to be proved")<<tr("Entering the initial premise and proving");

    drawLabelsId = *new vector<int>;
    A = "";
    B = "";

    PaintWidget *scrollwidget = new PaintWidget(this, view, &drawLabelsId);
    connect(this,SIGNAL(draw_clicked()),scrollwidget,SLOT(drawAline()));
    scrollwidget->setLayout(scrolllayout);
    scrollarea->setWidget(scrollwidget);

    setCentralWidget(scrollarea);

    QScreen* screen = QApplication::screens().at(0);
    QSize size = screen->availableSize();
    maxheight = size.height();
    size_window = 1200<=size.width()?QPoint(1200,200):QPoint(size.width(),200);
    createInputDialog();
}

//Метода back-end для компиляции формул
void MainWindow::updatePreviewBuilderThreadInput()
{
    // in linux, I need to reinstate the preamble when rendering. No idea why.
    input.preamble = QString("\\usepackage{amssymb,amsmath}");
    if(mPreviewBuilderThread->inputChanged(input)) {
        qDebug() << "input changed. Render...";
        ui->statusBar->showMessage("Ввод изменен, загрузка...");
        mPreviewBuilderThread->start();
  }
}

//В данном методе определяется, в какое поле(исходного утверждения, или может быть, доказательства)
//Заносить сгенерированную формулу
void MainWindow::showRealTimePreview(const QImage& preview, bool latexerror)
{
    if (latexerror) {
        if (isNeg)
            ui->statusBar->showMessage("Отрицание открыто");
        else
            ui->statusBar->showMessage("Ошибка.");
    } else {
      pixmap = QPixmap::fromImage(preview);
      if(view->basicDone && view->basicDone->isEnabled())
      {
          QPixmap temp = pixmap.scaled(10000,view->basic->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
          view->basic->setFixedWidth(temp.width());
          view->basic->setPixmap(temp);
      }else if(view->aDone && view->aDone->isEnabled())
      {
          QPixmap temp = pixmap.scaled(10000,view->aLabel->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
          view->aLabel->setFixedWidth(temp.width());
          view->aLabel->setPixmap(temp);
      }
      else if(view->bDone &&view->bDone->isEnabled()) {
          QPixmap temp = pixmap.scaled(10000,view->bLabel->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
          view->bLabel->setFixedWidth(temp.width());
          view->bLabel->setPixmap(temp);
      }
      else if(view->splitCount!=0 && view->doneSplit && view->doneSplit->isEnabled()){
          QPixmap temp = pixmap.scaled(10000,view->labelsOfSplits.at((view->splitCount-1) INT)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
          view->labelsOfSplits.at((view->splitCount-1)INT)->setFixedWidth(temp.width());
          view->labelsOfSplits.at((view->splitCount-1)INT)->setPixmap(temp);
      }
      else {

          if(!view->isLabelVerified.at(view->currentFocus INT)){
              pixmap = changeColor(pixmap,'r');
              QPixmap temp = pixmap.scaled(10000,view->labels.at(view->currentFocus INT)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
              view->labels.at(view->currentFocus INT)->setFixedWidth(temp.width());
              view->labels.at(view->currentFocus INT)->setPixmap(temp);
          }
      }
    }
}

//Функция компиляции latex по переданной строке
void MainWindow::updateLatex(QString *latex){
    input.preamble = QString("\\usepackage{amssymb,amsmath}");
    input.latex = *latex;
    mPreviewBuilderThread->inputChanged(input);
    mPreviewBuilderThread->start();
    qDebug()<<input.latex;
}

//Метод вызова диалогового окна с выбором действий
void MainWindow::createInputDialog(){
        bool ok;
        QString item = QInputDialog::getItem(this, tr("Choose the action"),
                                             tr("Action:"), items, 0, false, &ok);
        if (ok && !item.isEmpty())
            qDebug()<<item;

    //Пользователь в диалоговом окне выбирает нужно дейтсвие, ниже происходит добавление элементов сцены в зависимости от нужного действия
    if (ok && item == "Enter the basic statement"){//Пункт ввод исходного утверждения
    //BUG delete срабатывает на проверенные посылки

        //Вызов метода для создания элементов на сцене для ввода исходного утверждения
        view->createBasicStatement();

        //Пользователь вводит строку с клавиатуры, следующими двумя строками задается строка(чтобы каждый раз при отладке не вводить ее заново)
        //При выдаче программы пользователю, необходимо закомментировать эти строки
//        latex = "X_1\\cap X_2\\cap X_3=\\emptyset \\wedge X_2\\subseteq (X_1\\cup X_3)\\leftrightarrow \\overline{X_1\\cup X_2}\\cup (X_2\\cap \\overline{X_3})=(X_1\\cap X_2)\\cup (\\overline{X_1}\\cap \\overline{X_2})";
        latex= "\\overline{X_1\\cap \\overline{X_2}\\cup \\overline{X_3}}=(X_2\\cup X_3)\\cap \\overline{X_1}\\leftrightarrow X_1\\cap X_2\\cap X_3=\\emptyset \\wedge X_2\\subseteq (X_1\\cup X_3)";
        updateLatex(&latex);

        //После выбора действия "Ввод исходного утверждения" необходимо удалить этот пункт из меню, чтобы пользователь второй раз его не вызвал
        items.removeAt(items.indexOf("Enter the basic statement"));
        //Подстраиваем размер окна, под сцену
        resize(size_window.x(),size_window.y());
    }
    else if(ok && item =="Inputting identical transformations for some sets" && view->basic){ //Пункт Ввод тождественных преобразований для множеств

        //Задаем здесь строку, чтобы каждый раз не вводить ее руками
//        A= "\\overline{X_1\\cup X_2}\\cup (X_2\\cap \\overline{X_3})=(\\overline{X_1}\\cap \\overline{X_2})\\cup (X_2\\cap \\overline{X_3})";
        A= "\\overline{X_1\\cap \\overline{X_2}\\cup \\overline{X_3}}=(\\overline{X_1}\\cap X_3)\\cup (X_2\\cap X_3)";
        updateLatex(&A);

        //Вызов метода создания элементов на сцене для ввода тождественных преобразований
        view->createIdentificalTransformation();
        //Удаляем выбранный пункт меню
        items.removeAt(items.indexOf("Inputting identical transformations for some sets"));

        //Изменяем размер окна
        if(size_window.y()+200<=maxheight)
            size_window = QPoint(size_window.x(),size_window.y()+200);
        resize(size_window.x(),size_window.y());
    }
    else if(view->splitCount!=0 && item == "The choice of the statement to be proved" && view->splitGroupBox){//Пункт Выбор утверждения для последующего док-ва

        //Создаем нужные элементы на сцене
        view->createChoiceOfStatements();
        //Уделяем из меню
        items.removeAt(items.indexOf("The choice of the statement to be proved"));

        if(size_window.y()+100<=maxheight)
            size_window = QPoint(size_window.x(),size_window.y()+100);
        resize(size_window.x(),size_window.y());
        createInputDialog();
    }
    else if(ok && item== "Entering the initial premise and proving"  &&(view->labelsOfSplits.size()==view->splitCount-view->countAdded-1) ){
        //Пункт ввод начально посылки и проведение доказательства
        view->createBeginProving();
        items.append("The choice of the statement to be proved");
        if(size_window.y()+200<=maxheight)
            size_window = QPoint(size_window.x(),size_window.y()+200);
        resize(size_window.x(),size_window.y());
    }
    else if(ok&&item =="Splitting the basic statement" && view->basic){
        //Пункт разбиения исзоднго утверждения
        //Задаем первое разбиение для отладки
        view->createSplittingTheBasicStatement();
//        te = "X_1\\cap X_2\\cap X_3=\\emptyset \\wedge X_2\\subseteq (X_1\\cup X_3)\\rightarrow B\\subseteq A";
//        view->stringOfSplit.back() = &te;//Добавляем его во множество текущих разбиений
//        updateLatex(view->stringOfSplit.back());

        items.removeAt(items.indexOf("Splitting the basic statement"));
        if(size_window.y()+100<=maxheight)
            size_window = QPoint(size_window.x(),size_window.y()+100);
        resize(size_window.x(),size_window.y());
    }
    else createInputDialog();
}

//Обработка нажатия на кнопку вызова панели инструментов
void MainWindow::tool_clicked()
{
    if(!view->symbol){
        view->symbol = new symbols(this, &latex, ui->statusBar);
        connect(view->symbol, SIGNAL(update(QString*)), this, SLOT(updateLatex(QString*)));
        connect(view->symbol,SIGNAL(clear()), this, SLOT(clear()));
        view->moveDialog();
        view->symbol->show();
    }
    else view->symbol->show();
}

//Метод, вызываем пр нажатии на кнопку "очистить"
//Удаляем текущее содержимое строки и поля ввода
void MainWindow::clear(){
    if(view->basicDone->isEnabled())
    {
        view->basic->clear();
        latex.clear();
    }else if(view->aDone->isEnabled()) {
        view->aLabel->clear();
        A.clear();
    }
    else if(view->bDone->isEnabled()) {
        view->bLabel->clear();
        B.clear();
    }
    //BUG clear не срабатывает в окне разбиения
    else {
        view->labels.at(view->currentFocus INT)->clear();
        view->sendsInput.at(view->currentFocus INT).clear();
    }
    ui->statusBar->clearMessage();
}

//Метод, вызываемый при нажатии на кнопку проверки исходного утверждения (Зеленой галки)
void MainWindow::basic_done()
{
    if(!latex.isEmpty()){
        view->basicDone->setEnabled(false);//Отключаем кнопку, чтобы ее нельзя было нажать второй раз

        //Провереяем строку на правильность с помощью объекта Parser
        Parser *parser = new Parser(&latex,ui->statusBar);
        if(!(Parser::tableOfFunc(parser->getOPZ())))
            ui->statusBar->showMessage("Ошибка исходного утверждения.");
        else ui->statusBar->showMessage("Исходное утвреждение верно.");


        //Разделяем исходное утверждение для будущей проверки при разбиении
        //Проверка полученных разбиений
        splitsStringsFromBasic.push_back(QString());//(6)
        splitsStringsFromBasic.push_back(QString());//(7)
        splitsStringsFromBasic.push_back(QString());//(8)
        splitsStringsFromBasic.push_back(QString());//(9)

        QStringList left_and_right = latex.split("\\leftrightarrow ");
        if(left_and_right.size()!=2)
            ui->statusBar->showMessage("Ошибка, невозможно заменить на импликацию в обе стороны");

        //Если левая часть от эквиваленции содержит конъюнкцию
        //То делим на два утверждения с разными частями конъюнции
        if(left_and_right.front().contains("\\wedge ")){ //&& !left_and_right.front().contains("=\\emptyset") &&!left_and_right.front().contains("=U")){
            splitsStringsFromBasic.front() = left_and_right.back()+"\\rightarrow "+left_and_right.front().split("\\wedge ").front();
            splitsStringsFromBasic.at(1) = left_and_right.back()+"\\rightarrow "+left_and_right.front().split("\\wedge ").back();

            //Проверяем на вилидность правую часть
            qDebug()<<left_and_right.back().count("=");
            if(left_and_right.back().count("=")!=1){
                ui->statusBar->showMessage("Ошибка. Правая часть не содержит неверное число =.");
                return;
            }

           splitsStringsFromBasic.at(2) = left_and_right.front()+"\\rightarrow "+left_and_right.back().replace("=","\\subseteq ");
           QStringList left_and_right = latex.split("\\leftrightarrow ");
           splitsStringsFromBasic.at(3) = left_and_right.front()+"\\rightarrow "+left_and_right.back().split("=").back()+"\\subseteq "+left_and_right.back().split("=").front();

        }
        //Аналогично с правой частью
        else if(left_and_right.back().contains("\\wedge ")){// && !left_and_right.back().contains("=\\emptyset ") && !left_and_right.back().contains("=U")){
            splitsStringsFromBasic.front() = left_and_right.front()+"\\rightarrow "+left_and_right.back().split("\\wedge ").front();
            splitsStringsFromBasic.at(1) = left_and_right.front()+"\\rightarrow "+left_and_right.back().split("\\wedge ").back();


            //Проверяем на вилидность правую часть
            if(left_and_right.front().count("=")!=1){
                ui->statusBar->showMessage("Ошибка. Правая часть не содержит неверное число =.");
                return;
            }

            splitsStringsFromBasic.at(2)= left_and_right.back()+"\\rightarrow "+left_and_right.front().replace("=","\\subseteq ");
            QStringList left_and_right = latex.split("\\leftrightarrow ");
            splitsStringsFromBasic.at(3) = left_and_right.back()+"\\rightarrow "+left_and_right.front().split("=").back()+"\\subseteq "+left_and_right.front().split("=").front();
        }
    }
    createInputDialog();
}

//Метод вызывается при нажатии пользователем кнопки завершения ввода замены утверждения (A)
void MainWindow::aDone_clicked()
{
    bool error = false;
    if(!A.isEmpty()){
        view->aDone->setEnabled(false);
        //Проверка грамматики A
        if(!grammar(A).checkGrammar(SET)){
            ui->statusBar->showMessage("Ошибка грамматики A.");
            error = true;
            return;
        }

        //Проверка правильности A
        QList<QString> transforms;
        QList <QString> parts = latex.split("\\leftrightarrow");

        //Определение части исходного утверждения, в которой содержится равенство множеств
        if(parts.front().contains("=") && !parts.front().contains("=\\emptyset ") && !parts.front().contains("=U"))
            transforms = latex.split("\\leftrightarrow").at(0).split("=");
        else transforms = latex.split("\\leftrightarrow").at(1).split("=");

        //Получаем часть утверждения A до знака =, чтобы найти это же утверждение в исходном
        QString temp = A.split("=").at(0);
        A_old = A.split("=").at(0);
        Parser *parser = new Parser(&temp, ui->statusBar);
        temp = parser->getOPZ();

        //Выделяем две строки из части исходного утверждения, в которой находится равенство множеств
        //Одна из них должна соответсвовать утверждению A
        //А вторая B
        QString temp2 = transforms.at(0);
        QString temp3 = transforms.at(1);
        Parser *parser2 = new Parser(&temp2,ui->statusBar);
        Parser *parser3 = new Parser(&temp3,ui->statusBar);
        temp2 = parser2->getOPZ();
        temp3 = parser3->getOPZ();

        //Если введееная пользователем замена А не образует тождественно истиннной функции ни с чаасть А ни с B из исходного утвенрждения, то ошибка
        if(!(Parser::tableOfFunc(temp+temp2+"=") || Parser::tableOfFunc(temp+temp3+"="))){
            ui->statusBar->showMessage("Ошибка тождественного преобразования");
            error = true;
        }
        else {
            //Далее смотрим, если введенная замена образует тождественную функцию с частью исходного утверждения слева от равенства,то
            //Левая частть в исходном утверждении - это А, а правая -это B
            if(Parser::tableOfFunc(temp+temp2+"="))
               transforms2 = temp3;
            //Здесь наоборот
            else if(Parser::tableOfFunc(temp+temp3+"="))
                transforms2 = temp2;
            //Далее проверяем преобразования в замене А, введенные пользователем (Между знаками =)
            for(int i =1;i<A.split("=").size();i++){
                QString t = A.split("=").at(i);
                Parser *parser = new Parser(&t,ui->statusBar);
                if(!(Parser::tableOfFunc(temp+parser->getOPZ()+"="))){
                    ui->statusBar->showMessage("Ошибка тожд преобр 2");
                    error = true;
                }
            }
            if(A.contains("="))
                A = A.split("=").at(1);
            Parser *parserA = new Parser(&A,ui->statusBar);
            A = parserA->getOPZ();
        }

        if(!error)
            ui->statusBar->showMessage("Утверждение А верно.");

        //Создаем окно инструментов для записи в утверждение B
        if(view->symbol){
            view->symbol->~symbols();
            view->symbol =  nullptr;
        }
        view->symbol = new symbols(this,&B, ui->statusBar);
        connect(view->symbol, SIGNAL(update(QString*)), this, SLOT(updateLatex(QString*)));
        connect(view->symbol,SIGNAL(clear()), this, SLOT(clear()));
        view->moveDialog();
        view->symbol->show();

        //Задаем утверждение B
//        B = "(X_1\\cap X_2)\\cup (\\overline{X_1}\\cap \\overline{X_2})";
//        B = "(X_2\\cup X_3)\\cap \\overline{X_1}=X_2\\cap \\overline{X_1}\\cup X_3\\cap \\overline{X_1}";
        B= "(X_2\\cup X_3)\\cap \\overline{X_1}=(X_2\\cap \\overline{X_1})\\cup (X_3\\cap \\overline{X_1})";
        updateLatex(&B);
    }
}

//Аналогичный метод для проверки B
void MainWindow::bDone_clicked()
{
    //При проверке утверждения А, мы выделили строку transforms2, которая предполагается для замены B в исходном утверждении
    bool error = false;
    if(!B.isEmpty()){
        view->bDone->setEnabled(false);
        //Проверка грамматики B
        if(!grammar(B).checkGrammar(SET)){
            ui->statusBar->showMessage("Ошибка грамматики B.");
            error = true;
            return;
        }

        QString temp = B.split("=").at(0);
        B_old = B.split("=").at(0);
        Parser *parser = new Parser(&temp, ui->statusBar);
        temp = parser->getOPZ();
        if(!(Parser::tableOfFunc(temp+transforms2+"="))){
            ui->statusBar->showMessage("Ошибка тождественного преобразования B");
            error = true;
        }
        else {
            for(int i =1;i<B.split("=").size();i++){
                QString t = B.split("=").at(i);
                Parser *parser = new Parser(&t,ui->statusBar);
                if(!(Parser::tableOfFunc(temp+parser->getOPZ()+"="))){
                    ui->statusBar->showMessage("Ошибка тожд преобр B2");
                    error = true;
                }
            }
            Parser *parserB = new Parser(&B,ui->statusBar);
            B = parserB->getOPZ();
        }

        QList <QString> parts = latex.split("\\leftrightarrow ");
        QString equalsOfSets ="";
        condition = "";// Строка полученная выделением уловия(не равенства множеств) из исходного утверждения
        if(parts.size()!= 2){
            ui->statusBar->showMessage("Ошибка(размер разбиения эквиваленции !=2)");
            error = true;
        }
        //Следующим шагом делим исходное утверждение на равенство множеств и условие
        if(parts.front().contains("=") && !parts.front().contains("=\\emptyset ") && !parts.front().contains("=U")){
            equalsOfSets = parts.front();
            condition = parts.back();
        }else {
            equalsOfSets = parts.back();
            condition = parts.front();
        }

//        condition = condition.prepend("A=B\\rightarrow ");//(4)
//        Parser *con = new Parser(&condition,ui->statusBar);
//        condition = con->getOPZ().replace("A",A).replace("B",B);
//        if(!(Parser::tableOfFunc(condition))){
//            ui->statusBar->showMessage("Ошибка разбиения(При проверке двух импликаций)");
//            error = true;
//        }
        if(!error)
            ui->statusBar->showMessage("Утверждение B верно.");
        createInputDialog();
    }
}

//Обработка нажатия на кнопку "+" при добавлении следующего разбиения исходного утверждения
void MainWindow::plus_clicked(){
    //Проверка предыдущего высказывания на грамматику
    QString check = *view->stringOfSplit.back();
    QString alternative = "";
    //Получение альтернативной строки в другом порядке
    //В последуюзих действиях мы создаем альтернативную строку проврки в другом порядке
    //Например, если check= "A=B", то alternative = "B=A"
    //Это нужно для проверки введенной строки пользователем в любом порядке

    if(check.contains("\\wedge ")){
        alternative = check;
        int index_wedge = check.indexOf("\\wedge ");
        int index_arrow = check.indexOf("\\rightarrow");
        //Выделяем части между wedge и arrow, чтобы произвести их обмен
        QString part1 = alternative.left(index_wedge);
        QString part2 = alternative.left(index_arrow);
        part2 =  part2.right(part2.size() - index_wedge);
        part2.remove("\\wedge ");
        QString result = part2+"\\wedge "+part1+alternative.right(alternative.size()-index_arrow);
        alternative = result;
    }
    else if(check.left(check.indexOf("\\rightarrow ")).contains("=")){
        alternative = check;
        int index_arrow = check.indexOf("\\rightarrow ");
        int index_equal = alternative.left(index_arrow).indexOf("=");
        QString part1 = alternative.left(index_equal);
        QString part2 = alternative.left(index_arrow);
        part2 = part2.right(part2.size()-index_equal);
        part2.remove("=");
        QString result = part2+"="+part1+alternative.right(alternative.size()-index_arrow);
        alternative = result;
    }
    else{
        ui->statusBar->showMessage("Ошибка. Возможно Вы недостаточно разбили введенное простое утверждение, либо Вы сделали грамматическую ошибку");
        return;
    }
    //Производим замену для проверки строки
    if(check.contains("A"))
        check.replace("A",A_old);
    if(check.contains("B"))
        check.replace("B",B_old);
    if(alternative.contains("A"))
        alternative.replace("A",A_old);
    if(alternative.contains("B"))
        alternative.replace("B",B_old);
    bool haserror = true;
    //Проверка основного и альтернативного варианта на соответсвие с выделенными частями на этапе ввода исходного утверждения
    for(size_t i =0;i<splitsStringsFromBasic.size();i++){
        if(splitsStringsFromBasic.at(i) == check || splitsStringsFromBasic.at(i) == alternative){
            splitsStringsFromBasic.erase(splitsStringsFromBasic.begin() + i);
            haserror = false;
            break;
        }
    }
    ui->statusBar->showMessage(haserror? "Ошибка. Возможно Вы недостаточно разбили введенное простое утверждение, либо Вы сделали грамматическую ошибку":
                                         "Введенная часть исходного утверждения верна.");
    if(haserror)
        return;

    //Добавляем введенное пользователем разбиение
    view->hboxesOfSplits.push_back(new QHBoxLayout);
    view->hboxesOfSplits.back()->setAlignment(Qt::AlignLeft);

    view->labelsOfSplits.push_back(new QLabel);
    view->stringOfSplit.push_back(new QString(""));

    //Создаем окно инструментов (кнопок ввода)
    if(view->symbol){
        view->symbol->~symbols();
        view->symbol =  nullptr;
    }
    view->symbol = new symbols(this, view->stringOfSplit.at(view->splitCount INT), ui->statusBar);
    connect(view->symbol, SIGNAL(update(QString*)), this, SLOT(updateLatex(QString*)));
    connect(view->symbol,SIGNAL(clear()), this, SLOT(clear()));
    view->moveDialog();
    view->symbol->show();

    view->labelsOfSplits.at(view->splitCount INT)->setStyleSheet("QLabel {"
                                                 "min-width:420px;"
                                                 "min-height:30px;"
                                                 "max-height:50px;}");
    //Устанавливем в только что созданное поле красную точку ввода для пользователя
    QPixmap dot= QPixmap(":/rec/img/dot.png");
    view->labelsOfSplits.back()->setPixmap(dot.scaled(30,30,Qt::KeepAspectRatio,Qt::SmoothTransformation));

    view->numbersOfSplit.push_back(new QLabel(QString::number(view->splitCount+1)+")"));
    QFont font = view->numbersOfSplit.at(view->splitCount INT)->font();
    font.setPointSize(19);
    view->numbersOfSplit.at(view->splitCount INT)->setFont(font);

    //Добавляем созданные метки, нмоера и слои
    view->hboxesOfSplits.at(view->splitCount INT)->addWidget(view->numbersOfSplit.at(view->splitCount INT));
    view->hboxesOfSplits.at(view->splitCount INT)->addWidget(view->labelsOfSplits.at(view->splitCount INT));
    view->splitgrouplay->addLayout(view->hboxesOfSplits.at(view->splitCount INT));
    view->splitCount++;

    if(size_window.y()+100<=maxheight)
        size_window = QPoint(size_window.x(),size_window.y()+100);
    resize(size_window.x(),size_window.y());
}

//Метод, вызываемый при нажатии пользователем кнопки по завершении ввода всех разбиений исходного множества
void MainWindow::splits_clicked(){
    //Делаем кнопки "+" и "Готово" недоступными
    view->doneSplit->setEnabled(false);
    view->plus->setEnabled(false);
    //Проверка полученных разбиений
    QString valid = QString("");
    //Номера утверждений из Новосибирской статьи
    //Процедура проверки полноты и правильности разбиения описана в письме

    QString s1 = "";//(6)
    QString s2 = "";//(7)
    QString s3 = "";//(8)
    QString s4 = "";//(9)


    //Нахождение 6 и 7
    for(int i =0; i<view->stringOfSplit.size();i++){
        if(view->stringOfSplit.at(i)->contains("A=B\\rightarrow "))
            if(s1 == "")
                s1 = *view->stringOfSplit.at(i);
            else s2 = *view->stringOfSplit.at(i);
        else if(s3 == "")
            s3 = *view->stringOfSplit.at(i);
        else s4 = *view->stringOfSplit.at(i);
    }

    if((s1.contains("A")||s1.contains("B")||s2.contains("A")||s2.contains("B") || s3.contains("A")||s3.contains("B") ||s4.contains("A")||s4.contains("B"))&&(A.count()==0 || B.count()==0)){
        ui->statusBar->showMessage("Неизвестные обозначения A и B");
        return;
    }


    //Проверка 6 и 7
    if(s1 == "" || s2 =="" || s3 == "" || s4 == ""){
        ui->statusBar->showMessage("Ошибка при разбиении");
        createInputDialog();
        return;
    }
    Parser *s1p = new Parser(&s1,ui->statusBar);
    Parser *s2p = new Parser(&s2,ui->statusBar);
    s1 = s1p->getOPZ().replace("A",A).replace("B",B);
    s2 = s2p->getOPZ().replace("A",A).replace("B",B);
    QString valid1 = s1+s2+"*"+condition+"→";
    if(!Parser::tableOfFunc(valid1))
        ui->statusBar->showMessage("Ошибка");
    //Проверка 8 и 9
    Parser *s3p = new Parser(&s3,ui->statusBar);
    Parser *s4p = new Parser(&s4,ui->statusBar);
    s3 = s3p->getOPZ().replace("A",A).replace("B",B);
    s4 = s4p->getOPZ().replace("A",A).replace("B",B);
    valid = s3+s2+"*"+latex+"→";
    if(!Parser::tableOfFunc(valid1))
        ui->statusBar->showMessage("Ошибка 2");
    else
        ui->statusBar->showMessage("Разбиение верно.");
    createInputDialog();
}

//Метод вызывется при нажатии на кликабельное поле(объект ClickableLabel)
void MainWindow::labelClicked (QMouseEvent *event) {
    //узнать, от какой из меток пришёл сигнал:
    int preCur = view->currentFocus;
    view->currentFocus=((ClickableLabel *)sender())->tag();

    if((view->currentFocus)==-1 || (view->currentFocus)==-2)
        return;
    //Проверим, не проверена ли метка
    if(!view->isLabelVerified.at(view->currentFocus))
    {
        if(view->symbol){
            view->symbol->~symbols();
            view->symbol =  nullptr;
        }
        view->symbol = new symbols(this,&view->sendsInput.at(view->currentFocus INT), ui->statusBar);
        connect(view->symbol, SIGNAL(update(QString*)), this, SLOT(updateLatex(QString*)));
        connect(view->symbol,SIGNAL(clear()), this, SLOT(clear()));
        view->moveDialog();
        view->symbol->show();
    }

    //Задаем красную обводку для метки, на которую нажал пользователь
    view->labels.at(view->currentFocus INT)->setStyleSheet("QLabel{"
                                               "min-height:30px;"
                                               "max-height:30px;"
                                               "border-style: solid;"
                                               "border-width: 1px;"
                                               "border-color: red; "
                                               "}");
    //Задаем стиль для оставшихся меток без обводки
    for(int i = 0; i<int(view->labels.size());i++)
        if(i!=view->currentFocus)
            view->labels.at(i INT)->setStyleSheet("QLabel{"
                                            "min-height:30px;"
                                            "max-height:30px;"
                                            "}");
}

//Метод обработки нажатия на метку в диалоговом окне выбора утверждения для доказательства
void MainWindow::labelSelectProvenClicked (QMouseEvent *event) {
 //узнать, от какой из меток пришёл сигнал:
    view->selectedFocus=((ClickableLabel *)sender())->tag();
    view->dlg->close();
}

void MainWindow::drawAline(){
    emit draw_clicked();
}

//Метод, вызываемый при нажатии кнопки "Готово" по окончании доказательства
//В данном методе добавляются узлы в дерево для последуюзей проверки
void MainWindow::done1_clicked(){ 
    int i,index;
    //TODO Необходимо реализоватьп проверку количества стрелок
    i =index = view->findNonVerified();//Находим индекс последней непроверенной метки

    for(;i<int(view->labels.size()); i++){
        try{
            //Получаем  Обратную Польскую Запись
            Parser *parser = new Parser(&view->sendsInput.at(i),ui->statusBar);
            //И заносим результат в лист дерева соответсвующим индексом
            view->tree->at(i)->value=parser->getOPZ();
        }
        catch(exception e){qDebug()<<"Ошибка 1232.";}
    }


    //Обработка стрелок
    for(int i = 0; i<int(drawLabelsId.size())-1;i+=2){
        if(drawLabelsId.at(i)<index && drawLabelsId.at(i+1)<index)
            continue;
        try{
            if(drawLabelsId.at(i) == -1){
                //Если стрелка идет от левой части доказываемого утверждения
                node *temp = view->tree->at(drawLabelsId.at(i+1));
                view->provenNode->outputArrows.push_back(temp);
                view->tree->at(drawLabelsId.at(i+1))->inputArrow = view->provenNode;   //Заносим стрелку в дерево посылок

            }
            else if(drawLabelsId.at(i) == -2){
                //Если стрелка идет от правой части доказываемого утверждения
                view->proven2Node->outputArrows.push_back(view->tree->at(drawLabelsId.at(i+1)));

                view->tree->at(drawLabelsId.at(i+1))->inputArrow = view->proven2Node;//Заносим стрелку в дерево посылок
            }
            else {//Если стрелка идет от метки внутри доказательства(не от доказываемого утверждения)
                view->tree->at(drawLabelsId.at(i))->outputArrows.push_back(view->tree->at(drawLabelsId.at(i+1)));
                view->tree->at(drawLabelsId.at(i+1))->inputArrow=view->tree->at(drawLabelsId.at(i));
            }
        }
        catch(exception e){qDebug()<<"Ошибка 1233";}
    }
    //Проверка таблицей истинности
    checkbool(index);
}

//Метод осуществляет проверку узлов уже построенного дерева на предыдущем шаге
bool MainWindow::checkbool(int index){
    //В доказаетельстве непроверенные пылки пишутся красным цветом, а проверенные меняются на белый

    std::vector<QString *> stringsForCheck;//Множество строк, которые будут пропущены через таблицу истинности для проверки правильности
    QString valid= "";//Текущая проверяемая строка
    for(int i = index; i<view->labels.size();i++){
        if(i == 0){
            //Если на проверку подана первая метка(То есть начальная послыка),
            //То необходимо проверить грамматику на соответствие начальной посылки
            if(!grammar(view->sendsInput.at(i)).checkGrammar(SOURCE_PREMICE)){
                ui->statusBar->showMessage("Ошибка грамматики начлаьной посылки.");
                return false;
            }
            //Выделяем t - целевое утверждение в доказательстве
            QString t;
            if(view->splitlabel=="A\\subseteq B" ||view->splitlabel=="B\\subseteq A"){
                t= view->splitlabel.replace("\\subsetec ","").at(0);
            }
            else t = view->splitlabel;

            QString valid;
            Parser *parser = new Parser(&t,ui->statusBar);
            t = parser->getOPZ();//Получеам его ОПЗ

            if(view->tree->value.contains("$"))//Если начальная посылка содержит символ "Предположим Противное"
            {
                view->tree->value.replace("$","");
                valid.append(t+view->tree->value+"~"+"=");
            }
            else valid.append(t+view->tree->value+"=");
            //Заменяем символы А и B в проверяемой строке на соответсвующие им утверждения
            valid.replace("A",A);
            valid.replace("B",B);

            //Проверяем полученную строку
            bool res = Parser::tableOfFunc(valid);
            if(res){//Если строка верна, то красим посылка в красный цвет
                input.latex = view->sendsInput.at(i INT);
                KLFBackend::klfOutput out = KLFBackend::getLatexFormula(input, settings);
                view->labels.at(i INT)->setPixmap(QPixmap::fromImage(out.result).scaled(view->labels.at(i INT)->geometry().width(),view->labels.at(i INT)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                view->isLabelVerified.at(i INT) = true;//Говорим, что метка с номером i проверена верно
                continue;
            }
            else ui->statusBar->showMessage("Error, bad first premise.");
            return false;
        }
        //Если проверяется не начальная послыка
        //То проверяем ее грамматику либо на "Элементарный вывод", либо на "Конец ветви доказательства"
        if(!grammar(view->sendsInput.at(i)).checkGrammar(CONCLUSION) && !grammar(view->sendsInput.at(i)).checkGrammar(END_OF_BRANCH)){
            ui->statusBar->showMessage("Ошибка грамматики вывода.");
            return false;
        }

        stringsForCheck.push_back(new QString(""));
        //Если текущий узел содержит разбиение на случаи
        if(view->tree->at(i)->father->bracket=="->||"){
            stringsForCheck.back()->append(view->tree->at(i)->value);
            for(node *bros:view->tree->at(i)->brothers){
                stringsForCheck.back()->append(bros->value);
                stringsForCheck.back()->append("+");
            }
            stringsForCheck.back()->append(view->tree->at(i)->father->value);
            if(view->tree->at(i)->father->labelID==0)
                stringsForCheck.back()->append("=");
            else stringsForCheck.back()->append("→");
            //Проверяем правльность полученной строки
            if(checkPremise(*stringsForCheck.back())){
                //Если проверка прошла успешно, то красим метку в белый цвет
                input.latex = view->sendsInput.at(i INT);
                KLFBackend::klfOutput out = KLFBackend::getLatexFormula(input, settings);
                view->labels.at(i INT)->setPixmap(QPixmap::fromImage(out.result).scaled(view->labels.at(i INT)->geometry().width(),view->labels.at(i INT)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                view->isLabelVerified.at(i INT) = true;
                //Проверка братьев на грамматику
                for(int j = 0;j<view->tree->at(i)->brothers.size();j++){
                    if(view->sendsInput.at(i+j+1)=="")
                        break;
                    if(!grammar(view->sendsInput.at(i+j+1)).checkGrammar(CONCLUSION)&& !grammar(view->sendsInput.at(i)).checkGrammar(END_OF_BRANCH)){
                        ui->statusBar->showMessage("Ошибка грамматики вывода.");
                        return false;
                    }
                }
                //Красим братьев в белый цвет, как проверенные
                for(node *brother:view->tree->at(i)->brothers){
                    input.latex = view->sendsInput.at(brother->labelID);
                    KLFBackend::klfOutput out = KLFBackend::getLatexFormula(input, settings);
                    view->labels.at(brother->labelID)->setPixmap(QPixmap::fromImage(out.result).scaled(view->labels.at(brother->labelID)->geometry().width(),view->labels.at(brother->labelID)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                    view->isLabelVerified.at(brother->labelID) = true;
                }
            }
            i+=view->tree->at(i)->brothers.size();
        }
        //Аналогичная проверка только для разбтиения другого типа
        else if(view->tree->at(i)->father->bracket=="->{"){
            stringsForCheck.back()->append(view->tree->at(i)->value);
            for(node *bros:view->tree->at(i)->brothers){
                stringsForCheck.back()->append(bros->value);
                stringsForCheck.back()->append("*");
            }
            stringsForCheck.back()->append(view->tree->at(i)->father->value);
            if(view->tree->at(i)->father->labelID==0)
                stringsForCheck.back()->append("=");
            else stringsForCheck.back()->append("→");
            input.latex = view->sendsInput.at(i INT);
            KLFBackend::klfOutput out = KLFBackend::getLatexFormula(input, settings);
            view->labels.at(i INT)->setPixmap(QPixmap::fromImage(out.result).scaled(view->labels.at(i INT)->geometry().width(),view->labels.at(i INT)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
            view->isLabelVerified.at(i INT) = true;
            //Проверка братьев на грамматику
            for(int j = 0;j<view->tree->at(i)->brothers.size();j++){
                if(view->sendsInput.at(i+j+1)=="")
                    break;
                if(!grammar(view->sendsInput.at(i+j+1)).checkGrammar(CONCLUSION) && !grammar(view->sendsInput.at(i)).checkGrammar(END_OF_BRANCH)){
                    ui->statusBar->showMessage("Ошибка грамматики вывода.");
                    return false;
                }
            }
            for(node *brother:view->tree->at(i)->brothers){
                input.latex = view->sendsInput.at(brother->labelID);
                KLFBackend::klfOutput out = KLFBackend::getLatexFormula(input, settings);
                view->labels.at(brother->labelID)->setPixmap(QPixmap::fromImage(out.result).scaled(view->labels.at(brother->labelID)->geometry().width(),view->labels.at(brother->labelID)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                view->isLabelVerified.at(brother->labelID) = true;
            }
            for(int j = i+1;j<view->tree->at(i)->brothers.size();j++)
                if(!grammar(view->sendsInput.at(j)).checkGrammar(CONCLUSION) && !grammar(view->sendsInput.at(i)).checkGrammar(END_OF_BRANCH)){
                    ui->statusBar->showMessage("Ошибка грамматики вывода.");
                    return false;
                }
            i+=view->tree->at(i)->brothers.size();
        }


        else if(view->tree->at(i)->father->bracket==""){//Если нет разветления
            if(!view->tree->at(i)->inputArrow){//Если нет стрелок в текущую проверяемую метку
                stringsForCheck.back()->append(view->tree->at(i)->father->value);
                stringsForCheck.back()->append(view->tree->at(i)->value);
                stringsForCheck.back()->append("→");
                if(checkPremise(*stringsForCheck.back())){
                    input.latex = view->sendsInput.at(i INT);
                    KLFBackend::klfOutput out = KLFBackend::getLatexFormula(input, settings);
                    view->labels.at(i INT)->setPixmap(QPixmap::fromImage(out.result).scaled(view->labels.at(i INT)->geometry().width(),view->labels.at(i INT)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                    view->isLabelVerified.at(i INT) = true;
                }
            }
            else {//Если есть стрелки
                //Этот код нужно раскомментарить при выдаче пользователю. Для отладки он отключен
                //В следующем закомментаренном блоке кода проверяется не лишние ли стрелки, путем отбрасывания по одной и проверке истинности
//                if(!(view->tree->at(i)->value=="0,")){
//                    QString extraTest ="";
//                    extraTest.append(view->tree->at(i)->father->value);
//                    extraTest.append(view->tree->at(i)->value);
//                    extraTest.append("→");
//                    if(checkPremise(extraTest)){
//                        ui->statusBar->showMessage("Лишняя посылка");
//                    }
//                }

                //Дособираем строку для проверки и проверяем ее правильность.
                if(view->tree->at(i)->value=="0,")//x включен в 0
                    stringsForCheck.back()->append(view->tree->at(i)->inputArrow->value+view->tree->at(i)->father->value+"*,");
                else if(view->tree->at(i)->inputArrow->value.contains("→"))//Включение
                     stringsForCheck.back()->append(view->tree->at(i)->inputArrow->value+view->tree->at(i)->father->value+"*"+view->tree->at(i)->value+"→");
                else if(view->tree->at(i)->inputArrow->value.contains("~"))//a*b*c=0
                    stringsForCheck.back()->append(view->tree->at(i)->inputArrow->value+view->tree->at(i)->father->value+"*"+view->tree->at(i)->value+"→");
                else if(view->tree->at(i)->inputArrow->value.contains("="))//Если равенство множеств
                    stringsForCheck.back()->append(view->tree->at(i)->inputArrow->value+view->tree->at(i)->father->value+"*"+view->tree->at(i)->value+"→");
                else if(view->tree->at(i)->value == "." || view->tree->at(i)->value==",")//Если в посылке только черный квадрат
                    stringsForCheck.back()->append(view->tree->at(i)->inputArrow->value+view->tree->at(i)->father->value+"*"+view->tree->at(i)->value);
                else stringsForCheck.back()->append(view->tree->at(i)->inputArrow->value+view->tree->at(i)->father->value+"*"+view->tree->at(i)->value+"→");//a*b*c=U
                if(checkPremise(*stringsForCheck.back())){
                    input.latex = view->sendsInput.at(i INT);
                    KLFBackend::klfOutput out = KLFBackend::getLatexFormula(input, settings);
                    view->labels.at(i INT)->setPixmap(QPixmap::fromImage(out.result).scaled(view->labels.at(i INT)->geometry().width(),view->labels.at(i INT)->geometry().height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
                    view->isLabelVerified.at(i INT) = true;
                }
            }
        }
    }
}

//Метод проверки полученной от предыдущей функции строки на тождественную истинность с помощью таблицы (истинности).
//Возвращает true ил false в зависимости от результатов проверки
bool MainWindow::checkPremise(QString check){//Проверка БФТИ

    if(check.contains("a!a+")|| check.contains("b!b+") || check.contains("c!c+")||
           check.contains("!aa+") || check.contains("!bb+") ||check.contains("!cc+")){
        return true;
    }

    if(check.contains(".")){//Если проверяемая строка содержит белый квадрат
        QChar end = check.at(check.indexOf(".")-1);
        check.replace("A",A);
        check.replace("B",B);
        check.replace(".","");
        bool res = Parser::tableOfFunc(check);
        if(res){
           QString t = view->splitlabel.split("\\subseteq ").at(1);
           if(t != end){
               ui->statusBar->showMessage("Ошибка");
               return false;
           }
        }
        countSquare++;//Увелчивем счетсчик концов ветвей
        //Если пользователь набрал нужное еосличетсво концов веток, то доказательство завершено
        if(countSquare == (view->countBranches)){
            ui->statusBar->showMessage("Proof is true.");
            unCheckLabel();
            if(view->splitCount>view->countAdded){
                end_of_branch();
            }else ui->statusBar->showMessage("Доказательство всех утверждений верно");
        }
        return true;
    }
    //Если черный квадрат
    if(check.contains(",")){//, заменяет черный квадрат
        check.replace("A",A);
        check.replace("B",B);
        check.replace(",","");
        //Знак тильда соответсвует отрицанию, если пользователь ввел черный квадрат, то необходимо
        //проверить правильность обратного утверждения
        check.append("~");
        bool res = Parser::tableOfFunc(check);
        if(res){
            countSquare++;
            if(countSquare == (view->countBranches)){
                ui->statusBar->showMessage("Proof is true");
                unCheckLabel();
                if(view->splitCount>view->countAdded){
                    end_of_branch();
                }else ui->statusBar->showMessage("Доказательство всех утверждений верно");
            }
            return true;
        }
    }
    //Если не черный и не белый квадрат, то просто проверяем правильность функции
    check.replace("A",A);
    check.replace("B",B);
    bool res = Parser::tableOfFunc(check);
    if(!res){
        ui->statusBar->showMessage("Ошибка");
        return false;
    }
    return true;
}

//Метод убирает красное обрамление со всех меток при завершении доказательства
void MainWindow::unCheckLabel(){
    for(int i = 0; i<int( view->labels.size());i++)
        view->labels.at(i INT)->setStyleSheet("QLabel{"
                                        "min-height:30px;"
                                        "max-height:30px;"
                                        "}");
}
//Метод удаляет лишние элементы со сцены по завершении доказательства
void MainWindow::end_of_branch(){
    delete view->comboBox;
    view->comboBox = nullptr;
    delete view->add;
    view->add = nullptr;
    delete view->draw;
    view->draw = nullptr;
    delete view->verify;
    view->verify = nullptr;
    unCheckLabel();
    createInputDialog();
    return;
}

MainWindow::~MainWindow()
{
    delete mPreviewBuilderThread;
    delete ui;
}

//Метод изменяет цвет шрифта сгенерированного latex-изображения
QPixmap MainWindow::changeColor(QPixmap p,char ch)
{
    QColor color;
    if(ch == 'r')
        color.setRgb(255,0,0);
    else if(ch=='b')
        color.setRgb(255,255,255);
     QImage tmp = p.toImage();
     for(int y = 0; y < tmp.height(); y++)
     {
       for(int x= 0; x < tmp.width(); x++)
       {
           if(ch == 'r'){
                if(tmp.pixelColor(x,y).black()==0)
                    tmp.setPixelColor(x,y,color);
           }
         else if(ch=='b'){
                if(tmp.pixelColor(x,y).red()==255)
                    tmp.setPixelColor(x,y,color);
           }
       }
     }
     pixmap = QPixmap::fromImage(tmp);
     return pixmap;
}
