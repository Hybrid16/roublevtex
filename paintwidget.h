//#ifndef PAINTWIDGET_H
//#define PAINTWIDGET_H
#pragma once
#include "mainwindow.h"
#include <QWidget>
#include <QPainter>
#include <QPushButton>
#include <math.h>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QDebug>
#include "view.h"
#include <QPainterPath>

using namespace std;

class PaintWidget:public QWidget
{
    Q_OBJECT
public:
    PaintWidget(MainWindow *mw,View *view,vector<int> *drawLabelsId):QWidget(){
        this->tempStartArrows = new vector<QPoint*>;
        this->tempCenters = new vector<QPoint*>;
        this->arrowPathes = new vector<QPainterPath*>;
        this->startArrows = new vector<QPoint*>;
        this->tempEndArrows = new vector<QPoint*>;
        this->endArrows = new vector<QPoint*>;
        this->view = view;
        this->drawLabelsId = drawLabelsId;
        this->centers = new vector<QPoint*>;
        this->tempArrowPathes = new vector<QPainterPath *>;
        this->mw = mw;
    }

protected slots:
    void paintEvent(QPaintEvent* event);//Метод рисования стрклок в окне доказательства
    double GetAngle(const double dx, const double dy);
    void wheelEvent(QWheelEvent *event);//Метод смещения стрелки при прокрутке колеса мыши
    void mousePressEvent(QMouseEvent *event);//Метод смещения стрелки при нажатии мыши
    void mouseMoveEvent(QMouseEvent *event);//Метод смещения стрелки при нажатии и перемещении мыши
    void mouseReleaseEvent(QMouseEvent *e);
    void drawAline();//Метод вызываемый пр нажатии кнопки "Draw" в окне доказательства
    void drawLabelClicked(QMouseEvent* e);//Метод обработки нажатия на метку с посылкой в окне доказаельства
private:
    MainWindow *mw;

    //Для рисования
    //Временные значения хранят данный до повторного нажатия кнопки (до сохранения срелки)
    //Множества пуей стрелок
    vector<QPainterPath *> *tempArrowPathes;
    std::vector<QPainterPath *> *arrowPathes;
    //Координаты центров стрелок
    std::vector<QPoint *> *centers;
    std::vector<QPoint *> *tempCenters;
    //Координаты начала и конца стрелок
    std::vector<QPoint*> *startArrows;
    std::vector<QPoint *> *tempStartArrows;
    std::vector<QPoint*> *endArrows;
    std::vector<QPoint *> *tempEndArrows;

    int isLine = -1;
    bool isDrawSelect = false;
    View *view;
    vector<int> *drawLabelsId;
    int preCurrent =-100;

    QPoint curCoord,preCurCoord;
    QPoint z;

};

//#endif // PAINTWIDGET_H
