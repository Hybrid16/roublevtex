#ifndef PARSER_H
#define PARSER_H
#include <QString>
#include <QVector>
#include <iostream>
#include <math.h>
#include <QStatusBar>


class Parser
{
public:
    Parser(QString *latex,QStatusBar*);
    QString getOPZ();
    static bool tableOfFunc(QString);//Проверка тождесвенной истинности
    static bool calculateOPZ(QString opz, int *);//Проверяет истинность ОПЗ по заданному набору переменных y
private:
    QString parsedString;
    QString parse(QString*);// Получение из Latex формулы булевой(Метод предобработки и перевода комманд в символы)
    int commandParse(QString, QString *);//Разбор команд latex, начинающихся с /(слеша)
    bool isOpenBefore = false;//открыто до эквивалентности
    bool isOpenAfter = false;//после
    std::vector<int> indexesOfNeg(QString);//Возвращает все индексы символа ~
    std::vector<int> indexes;//Результат работы предыдущей функции
    QString opz(QString);//По строке составляет обратную польскую запись
    QStatusBar *statusBar;
    QString op;
};

#endif // PARSER_H
