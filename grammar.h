#ifndef GRAMMAR_H
#define GRAMMAR_H
#include <QString>
#include <QVector>
#include "mainwindow.h"

//Перечисления для классификации типа выражения
enum nonTerminal { OPERATION, RELATION_SET, RELATION_ELEMENT_AND_SET, SET, BASIC_PREMICE, SOURCE_PREMICE ,FIRST_PREMICE_DIRECT,
                   FIRST_PREMICE_INDIRECT, PREMICE, CONCLUSION, END_OF_BRANCH, WHITE_SQUARE,BLACK_SQUARE, ERROR};

class grammar

{
public:

    grammar(QString str);
    bool checkGrammar(nonTerminal expect);//Возвращает булево значение, соответсвует ли ожидаемыей результат проверки выражения с полученным
private:
    QString str;
    bool checkBrackets();//Проверяет равенство открывающих и закрывающих скобок
    nonTerminal switch_single_term(QString, int *i);//Проверка терминалдьного символа

    //Процедура проъодит по всем символам в vec и возарашает принадлежит ли nonterminal
    bool check_in_vec(QString ,vector<QString>, int *);

    bool switch_combinations(nonTerminal, int *i);//Проверяет комбинации выражений
    nonTerminal checkSet(int* i);//Функция пропускает все множества и возврашает значение, на котором закончилось множество

};

#endif // GRAMMAR_H
