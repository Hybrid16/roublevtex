//Класс узла дерева
#include "node.h"
#include <iostream>
using namespace std;

node::node()
{
    value = nullptr;
    father= nullptr;
    brothers.clear();
    upperSon = nullptr;
    centerSon= nullptr;
    downSon = nullptr;
    inputArrow = nullptr;
}

node* node::findLastNode(int index){
    static node *res;
    if(labelID == index){
        res = new node();
        res = this;
        while(res->upperSon)
            if(res->upperSon->labelID == index){
                res = res->upperSon;
            }
        return res;
    }
    else {
        if(this->upperSon)
            res = upperSon->findLastNode(index);
        if(this->centerSon)
            res = centerSon->findLastNode(index);
        if(this->downSon)
            res = downSon->findLastNode(index);
    }
    return res;
}

node* node::at(int index){
    static node *res;
    if(labelID == index){
        res = new node();
        res = this;
        return res;
    }
    else {
        if(this->upperSon)
            res = upperSon->at(index);
        if(this->centerSon)
            res = centerSon->at(index);
        if(this->downSon)
            res = downSon->at(index);
    }
    return res;
}
