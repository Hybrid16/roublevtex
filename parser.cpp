//Класс парсера(разбора сроки с утверждением)
#include "parser.h"

Parser::Parser(QString *latex,QStatusBar *statusBar)
{
    this->statusBar = statusBar;
    parsedString = parse(latex);
    op = opz(parsedString);
}

//Метод предобработки и перевода комманд в символы
QString Parser::parse(QString *str){
    QString result = "";
    int i =0;
    QString copy = *str;
    QChar temp;
    copy.remove("\\exists ");
    copy.replace("x\\in ","");
    copy.replace("\\forall ","");
    bool isNotIn= false;
    if(copy.contains("x\\notin ")){
        copy.remove("x\\notin ");
        result.append("~(");
        isNotIn =true;
    }
    while(i<copy.count()){
        temp = copy.at(i);


        if(copy.at(i) == " "){
            i++;
            continue;
        }
        else if(copy.at(i) == "X"){
            if(copy.at(i+2) == "1")
                result.append("a");
            else if(copy.at(i+2) == "2")
                result.append("b");
            else if(copy.at(i+2) == "3")
                result.append("c");
            i+=3;
        }
        else if(copy.at(i)=="A"){
            result.append("A");
            i++;
        }
        else if(copy.at(i)=="B"){
            result.append("B");
            i++;
        }
        else if(copy.at(i) == "\\"){
            i+=commandParse(copy.right(copy.count() - i), &result);
        }
        else if(copy.at(i) == "="){
            result.append("=");
            i++;
        }
        else if(copy.at(i) == "U"){
            result.append("U");
            i++;
        }
        else if(copy.at(i) == "("){
            result.append("(");
            i++;
        }
        else if(copy.at(i) == ")"){
            result.append(")");
            i++;
        }
        else if(copy.at(i) == "}"){
            result.append(")");
            i++;
        }

    }
    isOpenAfter? result.append(")"):result.append("");
    isNotIn? result.append(")"):result.append("");
    return result;
}


int Parser::commandParse(QString str,QString *res){
    QVector<QVector<QString>> commands{{"\\cap","*"},
                                       {"\\cup","+"},
                                       {"\\wedge","&"},
                                       {"\\emptyset", "0"},
                                       {"\\backslash", "-"},
                                       {"\\overline{","~("},
                                       {"\\subseteq","<"},
                                       {"\\supseteq", ">"},
                                       {"\\in","→"},
                                       {"\\rightarrow","→"},
                                       {"\\square","."},
                                       {"\\blacksquare",","},
                                       {"\\Cap","$"}//ПП
                                      }; //Словарь команд
    QString temp1,temp2;
    //Проверка простейших команд
    for(int i = 0; i<commands.size(); i++)
    {
        temp1= str.left(commands.at(i)[0].count());
        temp2 = commands.at(i)[0];
        if(str.left(commands.at(i)[0].size()) == commands.at(i)[0]){
            res->append(commands.at(i)[1]);
            return commands.at(i)[0].count();
        }
    }

    if(str.left(10) == "\\textit{u}")
    {

        if(!res->contains("↔"))// До <->
        {
            res->insert(0,"(");
            res->append(")*(");
            isOpenBefore = true;
            return 10;
        }
        else {// и нашлось после <->
                    res->insert(res->indexOf("↔")+1,"(");
                    res->append(")*(");
                    isOpenAfter = true;
                    return 10;
        }
    }
    else if(str.left(15) == "\\leftrightarrow"){
        isOpenBefore? res->append(")↔"): res->append("↔");
        return 15;
    }
    return 0;
}
//Возвращает все индексы символа ~
std::vector<int> Parser::indexesOfNeg(QString str){
    std::vector<int> resultVector;
    QString temp;
    for(int i = 0; i<str.count(); i++){
        temp = str.at(i);
        if(str.at(i)=="~")
            resultVector.push_back(i);
    }
    return resultVector;
}

//Метод проверки тождесттвенной истинности строки через таблицу истинноси
bool Parser::tableOfFunc(QString op){
    bool Fval = true;
    int n = 3;
    int y[3];//массив булевых переменных
    y[0] = 0;
    y[1] = 0;
    y[2] = 0;
    int x[int(pow(2,n))];//массив чисел x
    for(int i= 0; i< pow(2,n); i++){ //1.Начальное значение индекса i = 0 для работы с числами x[ i ].
            //8.Увеличиваем i на 1. Если i<(pow(2,n))
        x[i] = i;//2.Ставим в соответствие i строке число x[ i ] = i.
        for(int j = 0; j<n; j++) {//3.Начальное значение индекса j = 0 для работы с битами y
                // 6.Увеличиваем j на 1. Если j< n
            y[n-1-j] = (x[i]&1);//4.По формуле, записываем в y[n-1-j] значение x[ i ] & 1
            x[i]>>=1;//5.Сдвигаем x[ i ] на 1 вправо
         }
            Fval = calculateOPZ(op,y);//7.Отправляем полученный набор y в булеву
            // функцию и выводим значения переменных и полученного значения
         if(!Fval)
         {
             QString message = "Утверждение ложно для аргуметов: ";
//             statusBar->showMessage(message.append(QString::number(y[0])+", ").append(QString::number(y[1])+", ").append(QString::number(y[2]))+".");
            std::cout << "Statement is false for arguments:";
            std::cout<<"y[0]=" << y[0] << ",y[1]=";
            std::cout<< y[1] << ",y[2]=" << y[2] << std::endl;
            return false;
            break;
            }
        }
        return true;
}
//Возвращает приоритетт операции
int PRIOR(QChar a)
{
    if (a =="(")
        return 1;
    else if(a=="=")
        return 0;
    else if(a=="↔"||a=="→")
        return -1;
    else if(a== "~")
        return 4;
    else if(a == "&")
        return 1;
//    else if(a=="<")
//        return 2;
    else return 3;
}
//По строке составляет обратную польскую запись
QString Parser::opz(QString str){
    if(str.contains("=0")){//Переделываем =0
        int index = str.indexOf("0");
        if(str.size()!=index+1 && str.at(index+1) == ")"){
            str.remove(index-1,2);
            str.insert(index-7,"~");
        }
        else {
            str.insert(index+1,")");
            str.insert(index-6,"(");
            str.remove(index,2);
            str.insert(index-6,"~");
        }
    }
    if(str.contains("=U"))//Убираем U
        str.remove("=U");
    if(str.contains("~")){//Заменаем унарный оператор ~
        std::vector<int> neg = indexesOfNeg(str);
        for(int i = 0; i<neg.size(); i++){
            if((str.size()>=neg[i]+2) &&(str[neg[i]+2] == "B"||str[neg[i]+2]=="A")){
                str.remove(neg[i],2);
                str.remove(neg[i]+1,1);
                str.insert(neg[i]+1,"~");
                for(int i=0;i<neg.size();i++)
                    neg[i]-=2;
            }
            else if(str[neg[i]+3] == ")"){
                str.remove(neg[i]+1,1);
                str.remove(neg[i]+2,1);
                str.replace(neg[i],1,"!");
                for(int i=0;i<neg.size();i++)
                    neg[i]-=2;
            }
            else if(str[neg[i]+2] == "~" &&(str.size()>=neg[i]+6) && str[neg[i]+6]==")"){
                str.remove(neg[i],4);
                str.remove(neg[i]+1,2);
                for(int i=0;i<neg.size();i++)
                    neg[i]-=6;
                i++;
            }
        }
    }
      std::vector<QChar> oper;
      QString outstring;
      QString a = str;
      QChar ch;
      int point = 0;
          /* Повтоpяем , пока не дойдем до '=' */
        for(int k = 0; k<str.count(); k++)
        {
        /* Если очеpедной символ - ')' */
            ch = a[k];
          if(a[k]==')')
                     /* то выталкиваем из стека в выходную стpоку */
          {
                  /* все знаки опеpаций до ближайшей */
            while((oper.back()!='(')){
                  /* откpывающей скобки */
                outstring[point++]= oper.back();
                oper.pop_back();
            }
                  /* Удаляем из стека саму откpывающую скобку */
            oper.pop_back();
            continue;
          }
                        /* Если очеpедной символ - буква , то */
          if((a[k]>='a'&&a[k]<='z')||a[k]=="!" ||a[k]=="A" || a[k]=="B"||a[k]=="." || a[k]=="," ||a[k]=="$" || a[k]=="0"){
                  /* пеpеписываем её в выходную стpоку */
              outstring[point++]=a[k].toLatin1();
              continue;
          }
                        /* Если очеpедной символ - '(' , то */
          if(a[k]=='('){
                  /* заталкиваем её в стек */
              oper.push_back('(');
              continue;
          }
          if(a[k]=='+'||a[k]=='-'||a[k]=='*'||a[k]=='<'|| a[k] == '>' || a[k] == '~' || a[k] == "↔" || a[k] == "=" || a[k] == "→" ||a[k] == "<" ||a[k]=="&")
          /* Если следующий символ - знак опеpации , то: */
          {
              if(!oper.empty())
                int p1= PRIOR(oper.back());
              int p2 = PRIOR(a[k]);
                    /* если стек пуст */
            if(oper.empty())
             /* записываем в него опеpацию */
                oper.push_back(a[k]);
                 /* если не пуст */
            else
    /* если пpиоpитет поступившей опеpации больше
                   пpиоpитета опеpации на веpшине стека */
            if(PRIOR(oper.back())<PRIOR(a[k]))
            /* заталкиваем поступившую опеpацию на стек */
                oper.push_back(a[k]);
                       /* если пpиоpитет меньше */
            else
            {
              while((!oper.empty())&&(PRIOR(oper.back())>=PRIOR(a[k]))){
    /* пеpеписываем в выходную стpоку все опеpации
                       с большим или pавным пpиоpитетом */
                    outstring[point++]=oper.back();
                    oper.pop_back();
          }
                    /* записываем в стек поступившую  опеpацию */
              oper.push_back(a[k]);
            }
            continue;
          }
        }
           /* после pассмотpения всего выpажения */
        while(!oper.empty()){
         /* Пеpеписываем все опеpации из */
            outstring[point++]=oper.back();
            oper.pop_back();
          }
              /* стека в выходную стpоку */
        return outstring;
}

//Проверяет истинность ОПЗ по заданному набору переменных y
bool Parser::calculateOPZ(QString op, int y[]){
    std::vector<int> stack;               //стек
      int n1, n2, res;
      QChar ch;
      for(int i = 0; i<op.count(); i++) {
        ch = op[i];
        if(op[i] == "a") stack.push_back(y[0]);
        else if(op[i] == "b") stack.push_back(y[1]);
        else if(op[i] == "c") stack.push_back(y[2]);
        else if(op[i] == "~"){
            n1 = stack.back();
            stack.pop_back();
            res = !n1;
            stack.push_back(res);
        }
        else if(op[i] == "!"){//Унарный оператор отрицания
            if(op[i+1] == "a"){
                stack.push_back(!y[0]);
                i++;
            }
            else if(op[i+1] == "b"){
                stack.push_back(!y[1]);
                i++;
            }
            else if(op[i+1] == "c"){
                stack.push_back(!y[2]);
                i++;
            }
        }
        else {
          n2 = stack.back();
          stack.pop_back();
          n1 = stack.back();
          stack.pop_back();
          if(op[i] == "+") res = n1||n2;
          else if(op[i] == "*" || op[i]=="&") res = n1&&n2;
          else if(op[i] == "-") res = n1&&!n2;
          else if(op[i] == "↔" || op[i] == '=') res = n1 == n2;
          else if(op[i] == "<" || op[i] == "→") res = n1<=n2;
          stack.push_back(res);
        }
      }
      return stack.back();
}
QString Parser::getOPZ(){
    return this->op;
}
