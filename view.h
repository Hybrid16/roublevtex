#ifndef VIEW_H
#define VIEW_H

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include "symbols.h"
#include <QStatusBar>
#include <QGroupBox>
#include <QObject>
#include <QDialog>
#include "clickablelabel.h"
#include "klfbackend.h"
#include <QComboBox>
#include "node.h"
#include <QScrollArea>

using namespace std;
class View: public QObject
{
     Q_OBJECT

public:
    QVBoxLayout *mainLayout;
    symbols *symbol;
    QWidget *parent;
    QString *latex;
    QString *A,*B;
    QStatusBar *statusBar;

    //Создание исходного утверждения
    QLabel *basic;//Поле, в котором генерируется изображение latex по строке
    QPushButton *basicDone;//Кнопка окончания ввода полдьзователем

    //Создание тождественного преобразования
    QLabel *aLabel; //Метка для утверждения A
    QLabel *bLabel; //Метка для утверждения B
    QPushButton *aDone; //Кнопка завершения ввода A
    QPushButton *bDone; //Кнопка завершения ввода B


    //Создание разбиения
    QGroupBox *splitGroupBox; //Общий group-box в который будут размещены все разбиения
    QVBoxLayout *splitgrouplay;
    int splitCount =0;//Счетсчик разбиений исходного множества
    //Для каждой строки разбиения мы храним горизонтальный слой расположения элементов
    //Саму строку разбиения
    //Сгенерированное изображение по строке
    //Номер разбиения
    vector<QHBoxLayout*> hboxesOfSplits;//Множество горизонтальных слоев для каждого разбиения
    vector<QLabel*> labelsOfSplits;//Множество сгенерированных меток разбиения(Latex изображений)
    vector<QString *> stringOfSplit;//Вектор строк разбиения исходного утверждения
    vector<QLabel *> numbersOfSplit;// Множество меток, отображающих номер разбиения
    QPushButton *plus;//Кнопка добавления разбиения исходного утверждения
    QPushButton *doneSplit;//Кнопка завершения ввода всех разбиений

    //Создание диалога выбор
    QDialog *dlg;
    std::vector<ClickableLabel *> selectProven;
    QString splitlabel;//Правая часть в доказываемом утверждении (после ->) т.е целевое утверждение доказаетльство
    int selectedFocus;//Индекс утверждения для доказательства из диалогового окна выбора утверждения для доказательства
    QList<QString> list;//список исходных посылок из доказываемого утверждения
    QGroupBox *proof;
    ClickableLabel *proven; //Метка первого условия доказываемого утверждения
    ClickableLabel *proven2;//Метка второго условия доказываемого утверждения
    QVBoxLayout *proofVBox;

    //Создание окна доказательства
    vector<ClickableLabel *> labels; //множество меток, в которых отображаются посылки доказательства
    vector<bool> isLabelVerified;//Множество ,где i-ый элемент равен 1, если метка под номером i из labels уже была проверена и 0 в противном случае
    vector<QString> sendsInput;
    QList<QHBoxLayout *>hLayouts;
    QList<QVBoxLayout *>vLayouts;
    int currentFocus;//Хранит значение поля, на которое пользователь нажал мышью
    QComboBox * comboBox;
    QPushButton* add;
    node *tree;//Дерево посылок, строящееся при доказательстве.
    node* provenNode;//Узел в дереве, соответсвующий доказываемому утверждению
    node* proven2Node;//Узел второго доказываемого утверждения, разделенного через конъюнкцию
    QPushButton * verify;
    QPushButton *draw;
    int countAdded = 0;//Количество добавленных на текущий момент доказательств

    //Add clicked
    int lastDelimetr;//Индекс метки, от которой в послдений раз произошло разветвление
    QLabel *bracket;//метка с { или ||
    int countBranches = 1;//Количество разветвлений||

    View(QString*,QString*, QString *,QWidget *,QVBoxLayout*,QStatusBar*, KLFBackend::klfInput*, KLFBackend::klfSettings *);
    void createBasicStatement();//Метод создания окна ввода исходного утверждения
    void createIdentificalTransformation();//Метод создания окна ввода тождественныъ преобразований
    void moveDialog();//Метод перемещает диалоговое окно инструментов в правую часть от главного окна
    void createSplittingTheBasicStatement();//Метод создания онка разбиения исходного утверждения
    void createChoiceOfStatements();//Метод создания окна выбора утверждения для последующего доказательства
    void createBeginProving();//Метод создание окна ввода начальной посылки и доказательства
    int findNonVerified();//Метод нахождения индекса первой непроверенной метки

private:
    KLFBackend::klfInput *input;
    KLFBackend::klfSettings *settings;

private slots:
    void add_clicked();

};

#endif // VIEW_H
