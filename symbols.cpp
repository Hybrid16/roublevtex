//Класс для создания панели с кнопками и обработки строки latex при их нажатии
//Этот класс связан с главным окном при помощи сигналов.
//При нажатии на нужную кнопку, вызывается сигнал "update" в главный класс и соответвующее окно ввода сроки меняет свое содержимое.
//Аналогично с сигналом очистки строки "clear"

#include "symbols.h"
#include "ui_symbols.h"
#include <QDebug>
#include "mainwindow.h"
#include <QtWidgets>

symbols::symbols(QWidget *parent, QString *latex,QStatusBar *statusBar) :
    QDialog(parent),
    ui(new Ui::symbols)
{
    this->statusBar = statusBar;
    this->latex = latex;
    ui->setupUi(this);
}

symbols::~symbols()
{
    delete ui;
}

char* multiply(std::string str, int mult){
    char *result = new char[300];
    for(int i= 0; i<mult; i++){
        result = std::strcat(result,str.c_str());
    }
    return result;
}

//Метод добавляет нужный символ к строке, проверяет условие отрицаний и вызываетт сигнал
void symbols::addSymbol(QString str){
    negativeCheck(str);
    if(!countSub)
        statusBar->showMessage("Done");
    emit update(this->latex);
}

void symbols::on_X1_clicked(){addSymbol("X_1");}
void symbols::on_X2_clicked(){addSymbol("X_2");}
void symbols::on_X3_clicked(){addSymbol("X_3");}
void symbols::on_inter_clicked(){addSymbol("\\cap ");}
void symbols::on_unite_clicked(){addSymbol("\\cup ");}
void symbols::on_rightarrow_clicked(){addSymbol("\\rightarrow ");}
void symbols::on_leftrightarrow_clicked(){addSymbol("\\leftrightarrow ");}
void symbols::on_belong_clicked(){addSymbol("\\in ");}
void symbols::on_sub_clicked(){addSymbol("\\backslash ");}

//Проверяет, есть ли открытые отрицания(дополнения) и если есть, то редактирует строку latex соответсвующим образом
void symbols::negativeCheck(QString str){
    if(countSub>0){
        int pos = latex->lastIndexOf(QChar('}'));
        *latex = latex->left(pos-countSub+1);

    }
    latex->append(str);
    for(size_t i= 0;i<countSub;i++)
        latex->append("}");
}

void symbols::on_negative_clicked(){
    addSymbol("\\overline{}");
    countSub++;
    QString message;
    message ="Открыто ";
    statusBar->clearMessage();
    QString status = statusBar->currentMessage();
    int index = status.lastIndexOf(";");
    if(index>25)
        status = status.left(index);
    statusBar->showMessage(status+message.append(QString::number(countSub)).append(" отрицаний(е)"));

}

void symbols::on_close_clicked()
{
    if(countSub)
    {
        countSub--;
        emit update(latex);
        createMessage();
    }
}

void symbols::on_lbrace_clicked(){addSymbol("(");}
void symbols::on_rbrace_clicked(){addSymbol(")");}
void symbols::on_equal_clicked(){addSymbol("=");}
void symbols::on_zero_clicked(){addSymbol("\\emptyset ");}
void symbols::on_universal_clicked(){addSymbol("U ");}

//Метод очистки выражения, вызывает сигнла clear
void symbols::on_clear_clicked()
{
    latex->clear();
    emit clear();
    countSub = 0;
}

void symbols::on_pushButton_clicked(){addSymbol("\\wedge ");}
void symbols::on_Cap_clicked(){addSymbol("\\Cap ");}

void symbols::on_removeCharacter_clicked()
{
    QVector<QString> commands{"\\cap ","\\cup ","\\emptyset ","\\backslash ","\\subseteq ","\\supseteq ",
    "\\rightarrow ", "\\leftrightarrow ", "\\in ", "\\backslash ","\\emptyset ","\\nearrow ","\\searrow ",
    "\\Cap ","\\supseteq ","\\subseteq ", "\\textit{u}", "\\exists ", "\\notin ", "\\forall ","\\square ","\\blacksquare ", "\\wedge "}; //Словарь коман

//Уаление символов не внутри скобок
    for(int  i= 0;i<commands.size();i++){
        if(latex->right(commands.at(i).size())==commands.at(i)){
            if(latex->size() == commands.at(i).size()){
                emit clear();
                return;
            }
            *latex = latex->left(latex->size()-commands.at(i).size());
            emit update(latex);
            return;
        }
    }
    //BUG вылетает при удалении знака равно
    if(latex->size()==0)
        return;
    if(latex->back() == "1" || latex->back() == "2" || latex->back() == "3"){ //Удаление X1 X2 X3
        if(latex->size() == 3){
            emit clear();
            return;
        }
        latex->remove(latex->size()-3,3);
    }
    else if(latex->back() == "("||
            latex->back() == ")"||
           latex->back() == "="){
        if(latex->size() == 1){
            emit clear();
            return;
        }
        latex->remove(latex->size()-1,1);
        emit update(latex);
        return;
    }
    else if(latex->back() == "\\"){
        latex->remove(latex->size()-2,2);
        emit update(latex);
        return;
    }
    else if(latex->back() == "x"||
            latex->back() == ":"||
            latex->back() == "A"||
            latex->back() == "B"){
        if(latex->size() == 1){
            emit clear();
            return;
        }
        latex->remove(latex->size()-1,1);
        emit update(latex);
        return;
    }
    else if(latex->back() == "}")
    {
        int countBrackets = countOfNearBrackets(this->latex);
        //overline{}
         latex->remove(latex->size()-countBrackets-3,3);
         if(latex->at(latex->size()-countBrackets-1)=="{")
         {
            if(latex->size()==11){
                emit clear();
                countSub= 0;
                return;
            }
            else
            {
                latex->remove(latex->size()-11-countBrackets+1,11);
                if(countSub == latex->count("{")+1)
                    countSub--;
                createMessage();
            }
        }
    }
    emit update(this->latex);
}

void symbols::createMessage(){
    QString message;
    if(countSub>0)
    {
        message ="Открыто ";
        statusBar->clearMessage();
        QString status = statusBar->currentMessage();
        status = status.left(status.lastIndexOf(";"));
        statusBar->showMessage(status+message.append(QString::number(countSub)).append(" отрицаний(е)"));
    }
    else
            statusBar->showMessage("Готов.");
}

int symbols::countOfNearBrackets(QString *str){// Возвращает количество рядомстоящих }
    int result = 0;
    int i;
    int index = str->lastIndexOf("}");
    if(index)
        for(i = index-1, result = 1; i>=0;i--){
            if(str->at(i) == "}") result++;
            else return result;
        }
    return result;
}

void symbols::on_exists_clicked()
{
    addSymbol("\\exists ");
}

void symbols::on_xButton_clicked()
{
    addSymbol("x");
}

void symbols::on_doubledots_clicked()
{
    addSymbol(":");
}

void symbols::on_notin_clicked()
{
    addSymbol("\\notin ");
}

void symbols::on_aButton_clicked()
{
    addSymbol("A");
}

void symbols::on_bButton_clicked()
{
    addSymbol("B");
}

void symbols::on_all_clicked()
{
    addSymbol("\\forall ");
}


void symbols::on_square_clicked()
{
    addSymbol("\\square ");
}

void symbols::on_blacksquare_clicked()
{
    addSymbol("\\blacksquare ");
}


void symbols::on_dotcoma_clicked()
{
    addSymbol(";");
}

void symbols::on_subsetec_button_clicked()
{
    addSymbol("\\subseteq ");
}
