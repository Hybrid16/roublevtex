//Класс проверки грамматики утверждений
//Имеет главный метод checkGrammar, возвращающий соответствует ли результат с ожидаемым
#include "grammar.h"
using namespace std;

grammar::grammar(QString str)
{
    this->str = str;
    this->str = this->str.remove('}');
}

//Возвращает булево значение, соответсвует ли ожидаемыей результат проверки выражения с полученным
bool grammar::checkGrammar(nonTerminal expect){
    if(!checkBrackets())
        return false;
    this->str = this->str.remove('(');
    this->str = this->str.remove(')');
    int i = 0;
    return switch_combinations(expect, &i);
}

//Проверяет равенство открывающих и закрывающих скобок
bool grammar::checkBrackets(){return (str.count('(') == str.count(')'));}

//Возвращает тип значения
nonTerminal grammar::switch_single_term(QString check, int* i){
    vector<QString> operations{"\\cap ","\\cup "};
    vector<QString> relation_element_and_set{"\\in " ,"\\notin "};
    //TODO Пропускать пробелы, } и {}
    vector<QString> relation_element{"=", "\\subseteq "};
    vector<QString> set{"X_1","X_2","X_3","A","B", "\\emptyset ",
                        "\\overline{X_1", "\\overline{X_2", "\\overline{X_3","\\overline{A","\\overline{B"
                       };
    vector<QString> first_premice_direct{"\\forall x\\in "};
    vector<QString> first_premice_indirect{"\\Cap \\exists x\\notin ", "\\Cap \\exists x\\in "};
    vector<QString> white_square{"\\square"};
    vector<QString> black_square{"\\blacksquare"};

    //Проверка на "Утверждение"
    if(check.front() == 'x'){
        *i = *i+1;
        return CONCLUSION;
    }
    if(check_in_vec(check,relation_element_and_set,i))
        return RELATION_ELEMENT_AND_SET;
    if(check_in_vec(check,operations,i))
        return OPERATION;
    if(check_in_vec(check,set,i))
        return SET;
    if(check_in_vec(check,relation_element,i))
        return RELATION_SET;
    if(check_in_vec(check,first_premice_direct,i))
        return FIRST_PREMICE_DIRECT;
    if(check_in_vec(check,first_premice_indirect,i))
        return FIRST_PREMICE_INDIRECT;
    if(check_in_vec(check,white_square,i))
        return WHITE_SQUARE;
    if(check_in_vec(check,black_square,i))
        return BLACK_SQUARE;

}

//Процедура проъодит по всем символам в vec и возарашает принадлежит ли nonterminal
bool grammar::check_in_vec(QString check,vector<QString> vec, int*i){
    for(QString str: vec){
        QString cut = check.left(str.size());
        if(str == cut){
            *i+=str.size();
            return true;
        }
    }
    return false;
}

//Проверяет комбинации выражений
bool grammar::switch_combinations(nonTerminal value,int *i){
    //TODO обработка пустой строки.
    switch(value)
    {
    case CONCLUSION:{
        if(switch_single_term(str.right(str.size()-*i),i)!=CONCLUSION)
            return false;
        else if(switch_single_term(str.right(str.size()-*i),i)!=RELATION_ELEMENT_AND_SET)
            return false;
        else if(checkSet(i)!=SET)
                return false;
        break;
    }
    case FIRST_PREMICE_DIRECT:{
        if(switch_single_term(str.right(str.size()-*i),i)!=FIRST_PREMICE_DIRECT)
            return false;
        else if(checkSet(i)!=SET)
                return false;
        break;
    }
    case FIRST_PREMICE_INDIRECT:{
        if(switch_single_term(str.right(str.size()-*i),i)!=FIRST_PREMICE_INDIRECT)
            return false;
        else if(checkSet(i)!=SET)
                return false;
        break;
    }
    case BASIC_PREMICE:{
        if(checkSet(i)!=RELATION_SET)
            return false;
        else if(checkSet(i)!=SET)
            return false;
        break;
    }
    case SOURCE_PREMICE:{
        if(switch_combinations(FIRST_PREMICE_DIRECT,i))
            return true;
        else{
            *i=0;
            if(switch_combinations(FIRST_PREMICE_INDIRECT,i))
                return true;
            else return false;
        }
    }
    case PREMICE:{
        if(switch_combinations(CONCLUSION,i))
            return true;
        else{
            *i=0;
            if(switch_combinations(BASIC_PREMICE,i))
                return true;
            else{
                *i=0;
                if(switch_combinations(SOURCE_PREMICE,i))
                    return true;
                else return false;
            }
        }
    }
    case END_OF_BRANCH:{
        if(switch_combinations(WHITE_SQUARE,i))
            return true;
        else{
            i=0;
            if(switch_combinations(BLACK_SQUARE,i))
                return true;
            else return false;
        }
    }
    case SET:{
        nonTerminal value;
        if((value = checkSet(i))!=SET)
            if(value != RELATION_SET)
                return false;
            else {
                switch_combinations(SET,i);
            }
        break;
    }
    }
    return true;
}

//Функция пропускает все множества и возврашает значение, на котором закончилось множество
nonTerminal grammar::checkSet(int *i){
    nonTerminal retVal;
    if((retVal = switch_single_term(str.right(str.size()-*i),i))!=SET)
        return ERROR;
    if(*i>=str.size())
        return SET;
    else if((retVal = switch_single_term(str.right(str.size()-*i),i))!=OPERATION)
        return retVal;
    else return checkSet(i);
}
